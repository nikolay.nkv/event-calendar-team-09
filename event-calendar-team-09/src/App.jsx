import { useState, useEffect, useContext } from 'react';
import { Routes, Route } from 'react-router-dom';
import { useAuthState } from 'react-firebase-hooks/auth';
import { auth } from './services/firebase';
import { AuthContext } from './context/AuthContext';
import { getUserData } from './services/userService';
import Register from './components/auth/Register';
import Login from './components/auth/Login';
import NavBar from './components/navigation/NavBar';
import { Home } from './pages/Home';
import Calendar from './components/calendar/MonthCalendar/Calendar';
import UserProfile from './pages/UserProfile';
import SearchUser from './components/contacts/SearchUsers';
import EventModal from './components/events/EventModal';
import ContactsList from './pages/Contacts';
import EventDetails from './components/events/EventDetails';
import EventsList from './components/events/Events';
import CalendarContext from './context/CalendarContext';
import Footer from './components/footer/Footer';

function App() {
  const { selectedEvent } = useContext(CalendarContext);
  const [user, loading] = useAuthState(auth);
  const [appState, setAppState] = useState({
    user,
    userData: null
  });

  const [darkMode, setDarkMode] = useState(false);

  const toggleDarkMode = () => {
    setDarkMode(!darkMode);
    document.documentElement.classList.toggle('dark', darkMode);
  };

  useEffect(() => {
    if (user === null) {
      setAppState({
        user: null,
        userData: null
      });
    } else {
      getUserData(user.uid)
        .then((snapshot) => {
          if (!snapshot.exists()) {
            throw new Error('Something went wrong!');
          }

          setAppState({
            ...appState,
            userData: snapshot.val()[Object.keys(snapshot.val())[0]]
          });
        })
        .catch((e) => alert(e.message));
    }
  }, [user]);

  if (loading) {
    return <p>Loading...</p>;
  }

  return (
    <AuthContext.Provider value={{ user, loading, setUser: setAppState }}>
      <NavBar darkMode={darkMode} toggleDarkMode={toggleDarkMode}></NavBar>
      <Routes>
        {user !== null && <Route path="/calendar/:event-modal" element={<EventModal />} />}
        <Route path="/log-in" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="/home" element={<Home />} />
        <Route path="/calendar" element={<Calendar />} />
        <Route path="/userprofile" element={<UserProfile />} />
        <Route path="/search" element={<SearchUser />} />
        <Route path="/contacts" element={<ContactsList />} />
        {selectedEvent && <Route path="/event-details/:eventID" element={<EventDetails />} />}
        <Route path="/events" element={<EventsList />} />
      </Routes>
      <Footer></Footer>
    </AuthContext.Provider>
  );
}

export default App;
