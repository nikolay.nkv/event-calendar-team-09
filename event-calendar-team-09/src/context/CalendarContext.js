/* eslint-disable no-unused-vars */
import React from 'react';

const CalendarContext = React.createContext({
  dayIndex: 0,
  setDayIndex: () => {},
  weekIndex: 0,
  setWeekIndex: () => {},
  monthIndex: 0,
  setMonthIndex: () => {},
  selectedCategories: [],
  setSelectedCategories: () => {},
  smallCalendarMonth: 0,
  setSmallCalendarMonth: () => {},
  daySelected: null,
  setDaySelected: () => {},
  showEventModal: false,
  setShowEventModal: () => {},
  dispatchCalEvent: () => {},
  savedEvents: [],
  selectedEvent: null,
  setSelectedEvent: () => {},
  setLabels: () => {},
  labels: [],
  updateLabel: () => {},
  filteredEvents: [],
  calendarView: null,
  setCalendarView: () => {}
});

export default CalendarContext;
