import PropTypes from 'prop-types';

function AlertModal({ message, color }) {
  const iconColorClass = color === 'green' ? 'text-green-500' : 'text-red-500';
  const bgColorClass = color === 'green' ? 'bg-green-200' : 'bg-red-200';

  return (
    <div
      className={`fixed top-0 left-1/2 transform -translate-x-1/2 ${bgColorClass} p-5 rounded shadow-md shadow-zinc-400 dark:shadow-black flex items-center`}>
      <div className={`mr-4 ${iconColorClass} h-10 w-10 flex-shrink-0 rounded-full`}>
        {color === 'green' ? (
          <div className="alert-icon flex items-center bg-green-100 border-2 border-green-500 justify-center h-10 w-10 flex-shrink-0 rounded-full">
            <span className="text-green-500">
              <svg fill="currentColor" viewBox="0 0 20 20" className="h-6 w-6">
                <path
                  fillRule="evenodd"
                  d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
                  clipRule="evenodd"></path>
              </svg>
            </span>
          </div>
        ) : (
          <div className="alert-icon flex items-center bg-red-100 border-2 border-red-500 justify-center h-10 w-10 flex-shrink-0 rounded-full">
            <span className="text-red-500">
              <svg fill="currentColor" viewBox="0 0 20 20" className="h-6 w-6">
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"></path>
              </svg>
            </span>
          </div>
        )}
      </div>
      <div className="alert-content">
        <div className={`alert-title font-semibold text-lg text-${color}-800`}>
          {color === 'green' ? 'Added' : 'Removed'}
        </div>
        <div className={`alert-description text-sm text-${color}-600`}>{message}</div>
      </div>
    </div>
  );
}

AlertModal.propTypes = {
  message: PropTypes.string.isRequired,
  color: PropTypes.oneOf(['green', 'red']).isRequired
};

export default AlertModal;
