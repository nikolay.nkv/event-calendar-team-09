import { useState, useEffect } from 'react';
import { getAllUsers } from '../../services/userService';
import { addParticipants } from '../../services/eventService';
import PropTypes from 'prop-types';

export default function AddContact({ showModal, onClose }) {
  const [contacts, setContacts] = useState([]);
  const [selectedContacts, setSelectedContacts] = useState([]);

  useEffect(() => {
    const fetchContacts = async () => {
      const contactsRef = getAllUsers();
      contactsRef.on('value', (snapshot) => {
        const data = snapshot.val();
        if (data) {
          const contactList = Object.keys(data).map((key) => data[key]);
          setContacts(contactList);
        }
      });
    };
    fetchContacts();
  }, []);

  const toggleContactSelection = (contact) => {
    if (selectedContacts.includes(contact)) {
      setSelectedContacts(selectedContacts.filter((c) => c !== contact));
    } else {
      setSelectedContacts([...selectedContacts, contact]);
    }
  };

  const addSelectedContactsToEvent = () => {
    addParticipants(selectedContacts, event);
    onClose();
  };

  if (!showModal) {
    return null;
  }

  AddContact.propTypes = {
    showModal: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired
  };

  return (
    <div className="fixed inset-0 flex items-center justify-center z-50">
      <div className="bg-white p-6 rounded-lg shadow-md w-96">
        <h2 className="text-xl font-semibold mb-4">Select Contacts</h2>
        <ul className="space-y-2">
          {contacts.map((contact) => (
            <li key={contact.id} onClick={() => toggleContactSelection(contact)}>
              {contact.name}
            </li>
          ))}
        </ul>
        <button
          className="mt-4 bg-blue-500 hover:bg-blue-600 text-white px-4 py-2 rounded"
          onClick={addSelectedContactsToEvent}>
          Add Selected Contacts to Event
        </button>
      </div>
    </div>
  );
}
