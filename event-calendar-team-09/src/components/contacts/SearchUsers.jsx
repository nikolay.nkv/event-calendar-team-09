import {
  getUserByHandle,
  getUserData,
  getAllUsers,
  updateUserRoleInFirebase,
  blockUser,
  unblockUser
} from '../../services/userService';
import { useEffect, useState } from 'react';
import { useAuth } from '../../services/firebase';
import { toggleContact } from '../../services/userService';
import { storage } from '../../services/firebase';
import { getDownloadURL, ref } from 'firebase/storage';
import dark from '../../assets/dark.jpg';
import AlertModal from './alertModal';

export function SearchUser() {
  const currentUser = useAuth();
  const [userProfile, setUserProfile] = useState(null);
  const [searchTerm, setSearchTerm] = useState('');
  const [searchResults, setSearchResults] = useState([]);
  const [users, setUsers] = useState([]);
  const [isSearchActive, setIsSearchActive] = useState(false);
  const [profilePhotoUrls, setProfilePhotoUrls] = useState({});
  const [showAlert, setShowAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');
  const [alertColor, setAlertColor] = useState('');

  const updateUserRole = async (handle, newRole) => {
    try {
      if (!newRole) {
        console.error('New role is not defined.');
        return;
      }
      await updateUserRoleInFirebase(handle, newRole);

      const updatedUsers = users.map((user) =>
        user.handle === handle ? { ...user, role: newRole } : user
      );
      setUsers(updatedUsers);
    } catch (error) {
      console.error('Error updating user role:', error);
    }
  };

  const blockUserHandler = async (handle) => {
    try {
      await blockUser(handle);
      const updatedUsers = users.map((user) =>
        user.handle === handle ? { ...user, blocked: true } : user
      );
      setUsers(updatedUsers);
    } catch (error) {
      console.error('Error blocking user:', error);
    }
  };

  const unblockUserHandler = async (handle) => {
    try {
      await unblockUser(handle);
      const updatedUsers = users.map((user) =>
        user.handle === handle ? { ...user, blocked: false } : user
      );
      setUsers(updatedUsers);
    } catch (error) {
      console.error('Error unblocking user:', error);
    }
  };

  async function fetchUserProfile(handle) {
    try {
      const userProfileSnapshot = await getUserByHandle(handle);
      return userProfileSnapshot.val();
    } catch (error) {
      console.error('Error fetching user profile:', error);
      return null;
    }
  }

  useEffect(() => {
    async function fetchUserProfileData() {
      if (currentUser) {
        const fetchedUserProfile = await fetchUserProfile(currentUser.handle);
        setUserProfile(fetchedUserProfile);
      }
    }

    fetchUserProfileData();
  }, [currentUser]);

  useEffect(() => {
    if (currentUser) {
      getUserData(currentUser.uid)
        .then((snapshot) => {
          if (snapshot.exists()) {
            const userData = snapshot.val();
            const firstUser = Object.values(userData)[0];
            setUserProfile(firstUser);
          }
        })
        .catch((error) => {
          console.error('Error fetching user data:', error);
        });
    }
  }, [currentUser]);

  useEffect(() => {
    async function fetchUserData() {
      const userData = await getAllUsers();
      setUsers(userData);
    }
    fetchUserData();
  }, []);

  useEffect(() => {
    const filteredUserResults = users.filter((user) => {
      return (
        (user.firstName && user.firstName.toLowerCase().includes(searchTerm.toLowerCase())) ||
        (user.lastName && user.lastName.toLowerCase().includes(searchTerm.toLowerCase())) ||
        (user.handle && user.handle.toLowerCase().includes(searchTerm.toLowerCase())) ||
        (user.email && user.email.toLowerCase().includes(searchTerm.toLowerCase()))
      );
    });
    setSearchResults(filteredUserResults);
  }, [users, searchTerm]);

  useEffect(() => {
    if (isSearchActive) {
      const fetchProfilePhotoUrls = async () => {
        const urls = {};
        for (const user of searchResults) {
          if (user.uid) {
            try {
              const userPhotoRef = ref(storage, `${user.uid}.png`);
              const url = await getDownloadURL(userPhotoRef);
              urls[user.uid] = url;
            } catch (error) {
              console.error(`Error fetching profile photo for ${user.uid}:`, error);
            }
          }
        }
        setProfilePhotoUrls(urls);
      };

      fetchProfilePhotoUrls();
    }
  }, [isSearchActive, searchResults]);

  const handleToggleContact = async (contactHandle, isContact) => {
    if (contactHandle === userProfile.handle) {
      return;
    }

    if (isContact) {
      await toggleContact(userProfile.handle, contactHandle, 'remove');
      setAlertMessage(`${contactHandle} was removed from your contacts!`);
      setAlertColor('red');
    } else {
      await toggleContact(userProfile.handle, contactHandle, 'add');
      setAlertMessage(`${contactHandle} was added to your contacts!`);
      setAlertColor('green');
    }

    setShowAlert(true);

    setTimeout(() => {
      setShowAlert(false);
    }, 3000);
  };

  return (
    <>
      <div className="bg-zinc-200 dark:bg-zinc-800 min-h-screen pt-4 pb-10">
        <div className="flex items-center w-1/3 p-2.5 ml-8 rounded-full shadow shadow-zinc-400 dark:shadow-zinc-700 focus-within:shadow-teal-600 dark:focus-within:shadow-teal-600 bg-white dark:bg-zinc-900 overflow-hidden">
          <div className="grid place-items-center h-full w-12 text-gray-300">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-6 w-6"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor">
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
              />
            </svg>
          </div>
          <input
            className="outline-none ml-1 block bg-white dark:bg-zinc-900 text-zinc-700 dark:text-zinc-300 dark:placeholder-zinc-500 placeholder-zinc-500"
            type="text"
            id="search"
            placeholder="Search..."
            value={searchTerm}
            onFocus={() => setIsSearchActive(true)}
            onChange={(e) => setSearchTerm(e.target.value)}
            autoComplete="off"
          />
        </div>
        <div className="grid grid-cols-1 gap-2 md:grid-cols-2 lg:grid-cols-3 ml-4 mr-4 mt-4">
          {isSearchActive &&
            searchResults.map((user, index) => (
              <div key={index} className=" justify-center items-center mb-2">
                <div className="relative flex flex-col items-center rounded-[20px] w-[450px] mx-auto p-4 bg-zinc-100 dark:bg-zinc-900 bg-clip-border shadow-md pb-8 pt-4 shadow-zinc-400 dark:shadow-inner dark:shadow-zinc-950 dark:!bg-navy-800 dark:text-zinc-100">
                  <div className="relative flex h-32 w-full justify-center rounded-xl bg-cover">
                    <img
                      src={dark}
                      className="absolute flex h-32 w-full justify-center rounded-xl bg-cover"
                    />
                    <div className="absolute -bottom-12 flex h-[120px] w-[120px] items-center justify-center rounded-full border-[4px] border-zinc-100 dark:border-zinc-900">
                      <img
                        className="h-full w-full rounded-full"
                        src={
                          profilePhotoUrls[user.uid] ||
                          'https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png'
                        }
                        alt="Profile"
                      />
                    </div>
                  </div>
                  <div className="mt-16 flex flex-col items-center">
                    <h4 className="text-xl font-bold text-zinc-700 dark:text-zinc-200">
                      {' '}
                      {user.firstName} {user.lastName}
                    </h4>
                    <p className="text-base font-normal text-zinc-600 dark:text-zinc-400">
                      @{user.handle}
                    </p>
                    <p className="text-base font-normal text-zinc-600 dark:text-zinc-400">
                      {user.role}
                    </p>
                  </div>
                  <div className="mt-6 flex-row gap-14 md:!gap-14">
                    <div className="flex flex-row items-center justify-center mb-2">
                      <p className="text-sm font-bold text-zinc-600 dark:text-zinc-300">Email:</p>
                      <p className="text-sm font-normal text-zinc-500 dark:text-zinc-400 ml-1">
                        {user.email}
                      </p>
                    </div>
                    <div className="flex flex-row items-center justify-center">
                      <p className="text-sm font-bold text-zinc-600 dark:text-zinc-300">
                        Phone number:
                      </p>
                      <p className="text-sm font-normal text-zinc-500 dark:text-zinc-400 ml-1">
                        {user.phoneNumber}
                      </p>
                    </div>
                  </div>
                  <div className="flex items-center justify-center gap-4 mt-4 mb-2">
                    {userProfile.role === 'admin' && user.role === 'regular' && (
                      <button
                        className=" bg-zinc-200 transition duration-150 ease-in-out hover:shadow-inner hover:shadow-zinc-400 dark:shadow-none dark:bg-zinc-600 dark:hover:bg-zinc-800 rounded text-cyan-500 px-6 py-2 text-sm font-bold"
                        onClick={() => updateUserRole(user.handle, 'admin')}>
                        Make Admin
                      </button>
                    )}
                    {userProfile.role === 'admin' && user.role === 'regular' && (
                      <div className="user-actions">
                        {user.blocked ? (
                          <button
                            className=" bg-zinc-200 transition duration-150 ease-in-out hover:shadow-inner hover:shadow-zinc-400 dark:shadow-none dark:bg-zinc-600 dark:hover:bg-zinc-800 rounded text-teal-500 px-6 py-2 text-sm font-bold"
                            onClick={() => unblockUserHandler(user.handle)}>
                            Unblock User
                          </button>
                        ) : (
                          <button
                            className=" bg-zinc-200 transition duration-150 ease-in-out hover:shadow-inner hover:shadow-zinc-400 dark:shadow-none dark:bg-zinc-600 dark:hover:bg-zinc-800 rounded text-rose-500 px-6 py-2 text-sm font-bold"
                            onClick={() => blockUserHandler(user.handle)}>
                            Block User
                          </button>
                        )}
                      </div>
                    )}
                  </div>
                  {user.handle !== userProfile.handle && (
                    <button
                      className={`relative rounded-full px-3 py-3 overflow-hidden group ${
                        userProfile.contacts && userProfile.contacts[user.handle]
                          ? 'bg-rose-500 hover:bg-gradient-to-r hover:from-rose-500 hover:to-rose-400 text-white hover:ring-2 hover:ring-offset-2 hover:ring-rose-400 hover:ring-offset-zinc-100 dark:hover:ring-offset-zinc-900'
                          : 'bg-teal-500 hover:bg-gradient-to-r hover:from-teal-500 hover:to-teal-400 text-white hover:ring-2 hover:ring-offset-2 hover:ring-teal-400 hover:ring-offset-zinc-100 dark:hover:ring-offset-zinc-900'
                      } transition-all ease-out duration-300 mt-4`}
                      onClick={() =>
                        handleToggleContact(
                          user.handle,
                          userProfile.contacts && userProfile.contacts[user.handle]
                        )
                      }>
                      {userProfile.contacts && userProfile.contacts[user.handle] ? (
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                          strokeWidth={1.5}
                          stroke="currentColor"
                          className="w-6 h-6">
                          <path strokeLinecap="round" strokeLinejoin="round" d="M19.5 12h-15" />
                        </svg>
                      ) : (
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                          strokeWidth={1.5}
                          stroke="currentColor"
                          className="w-6 h-6">
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M12 4.5v15m7.5-7.5h-15"
                          />
                        </svg>
                      )}
                    </button>
                  )}
                </div>
              </div>
            ))}
        </div>
        {showAlert && (
          <AlertModal
            message={alertMessage}
            onClose={() => setShowAlert(false)}
            color={alertColor}
          />
        )}
      </div>
    </>
  );
}

export default SearchUser;
