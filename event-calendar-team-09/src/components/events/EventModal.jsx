import { useState, useEffect, useContext } from 'react';
import { auth } from '../../services/firebase';
import { addEventToUser, getUserData } from '../../services/userService';
import {
  MAX_EVENT_TITLE_LENGTH,
  MIN_EVENT_TITLE_LENGTH,
  WEEK_DAYS
} from '../../services/constants';
import { addEvent } from '../../services/eventService';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import CalendarContext from '../../context/CalendarContext';
import AddEventParticipants from './AddEventParticipants';
import PropTypes from 'prop-types';

export default function EventModal({ onClose }) {
  const { setShowEventModal, selectedEvent, daySelected } = useContext(CalendarContext);

  const [event, setEvent] = useState(
    selectedEvent ?? {
      title: '',
      description: '',
      startDate: null,
      endDate: null,
      address: '',
      isPublic: false,
      author: auth.currentUser.handle,
      participants: [],
      category: 'party',
      selectedWeekdays: [],
      selectedMonthDays: [],
      recurrencePattern: []
    }
  );

  const [, setBlockedUser] = useState(false);
  const [isAddParticipantsOpen, setAddParticipantsOpen] = useState(false);
  const [currentUser, setCurrentUser] = useState({});
  const [eventParticipants, setEventParticipants] = useState([]);

  useEffect(() => {
    getUserData(auth.currentUser.uid)
      .then((snapshot) => {
        if (snapshot.exists()) {
          const userData = snapshot.val();
          const userAuthor = Object.values(userData)[0];
          setCurrentUser(userAuthor);
          if (!userAuthor.blocked) {
            setEvent({
              ...event,
              author: userAuthor.handle || ''
            });
          } else {
            setBlockedUser(true);
          }
        }
      })
      .catch((error) => {
        console.error('Error fetching user data:', error);
      });
  }, []);

  const handleWeekdayChange = (weekday) => {
    const updatedSelectedWeekdays = event.selectedWeekdays.includes(weekday)
      ? event.selectedWeekdays.filter((day) => day !== weekday)
      : [...event.selectedWeekdays, weekday];
    setEvent({
      ...event,
      selectedWeekdays: updatedSelectedWeekdays
    });
  };

  const handleMonthDayChange = (day) => {
    const updatedSelectedMonthDays = event.selectedMonthDays.includes(day)
      ? event.selectedMonthDays.filter((d) => d !== day)
      : [...event.selectedMonthDays, day];

    setEvent({
      ...event,
      selectedMonthDays: updatedSelectedMonthDays
    });
  };

  const renderMonthDayCheckboxes = () => {
    const monthDayOptions = Array.from({ length: (31 - 1) / 1 + 1 }, (_, index) => 1 + index * 1);
    return monthDayOptions.map((day) => (
      <label key={day}>
        <input
          type="checkbox"
          checked={event.selectedMonthDays.includes(day)}
          onChange={() => handleMonthDayChange(day)}
        />
        {day}
      </label>
    ));
  };
  const addParticipantsHandler = () => {
    setAddParticipantsOpen(true);
  };
  const renderWeekDayCheckboxes = () => {
    return WEEK_DAYS.map((day) => (
      <label key={day}>
        <input
          type="checkbox"
          checked={event.selectedWeekdays.includes(day)}
          onChange={() => handleWeekdayChange(day)}
        />
        {day}
      </label>
    ));
  };

  const updateEvent = (prop) => (e) => {
    setEvent({
      ...event,
      [prop]: e.target.value
    });
  };

  const handleStartDateChange = (date) => {
    setEvent({
      ...event,
      startDate: date
    });
  };

  const handleEndDateChange = (date) => {
    setEvent({
      ...event,
      endDate: date
    });
  };

  const handleEventSubmit = () => {
    const formattedStartDate = new Date(event.startDate).toISOString();
    const formattedEndDate = new Date(event.endDate).toISOString();
    if (!event.title) {
      return alert('Title is required!');
    }
    if (
      event.title.length < MIN_EVENT_TITLE_LENGTH ||
      event.title.length > MAX_EVENT_TITLE_LENGTH
    ) {
      return alert(
        `The title length must be between ${MIN_EVENT_TITLE_LENGTH} and ${MAX_EVENT_TITLE_LENGTH}!`
      );
    }
    if (!event.startDate) {
      return alert(`The start date is required!`);
    }
    if (!event.endDate) {
      return alert(`The end date is required!`);
    }
    const eventParticipantsWithCurrentUser = [...eventParticipants, currentUser.handle];
    addEvent(
      event.title,
      event.description,
      formattedStartDate,
      formattedEndDate,
      event.address,
      event.isPublic,
      event.author,
      eventParticipantsWithCurrentUser,
      event.category,
      event.selectedWeekdays,
      event.selectedMonthDays,
      event.recurrencePattern
    )
      .then((data) => {
        data.participants.forEach((participant) => {
          addEventToUser(participant, data.id);
        });

        setShowEventModal(false);
      })
      .then(() => {
        onClose(false);
      })
      .catch((error) => {
        console.error('Error during create event:', error);
      });
  };

  EventModal.propTypes = {
    onClose: PropTypes.func.isRequired
  };

  return (
    <div className="fixed inset-0 bg-black bg-opacity-60 flex items-center justify-center z-50">
      <div className="bg-zinc-100 dark:bg-zinc-800 p-8 rounded-xl w-2/3 overflow-y-auto">
        <h2 className="text-2xl font-semibold mb-4 text-zinc-800 dark:text-zinc-200">
          Create New Event
        </h2>
        <label className="text-zinc-700 dark:text-zinc-200" htmlFor="title">
          Title:
        </label>
        <input
          type="text"
          name="title"
          id="title"
          placeholder="Title"
          value={event.title}
          onChange={updateEvent('title')}
          className="mb-4 p-2 focus:border hover:border rounded-md w-full text-zinc-600 dark:text-zinc-200 bg-zinc-50 dark:bg-zinc-700 hover:border-teal-500 focus:border-teal-500 dark:hover:border-teal-400 dark:focus:border-teal-400 focus:outline-none shadow-inner shadow-zinc-500 dark:shadow-black placeholder-zinc-400"
        />
        <label className="text-zinc-700 dark:text-zinc-200" htmlFor="description">
          Description:
        </label>
        <textarea
          type="text"
          name="description"
          id="description"
          placeholder="Description"
          value={event.description}
          onChange={updateEvent('description')}
          className="mb-4 p-2 focus:border hover:border rounded-md w-full text-zinc-600 dark:text-zinc-200 bg-zinc-50 dark:bg-zinc-700 hover:border-teal-500 focus:border-teal-500 dark:hover:border-teal-400 dark:focus:border-teal-400 focus:outline-none shadow-inner shadow-zinc-500 dark:shadow-black placeholder-zinc-400"
        />
        <label className="text-zinc-700 dark:text-zinc-200 mr-2" htmlFor="startDate">
          Start
        </label>
        <DatePicker
          selected={event.startDate ? new Date(event.startDate) : new Date(daySelected)}
          onChange={handleStartDateChange}
          showTimeSelect
          timeFormat="HH:mm"
          timeIntervals={15}
          dateFormat="MMMM d, yyyy h:mm aa"
          className="mb-2 p-2 focus:border hover:border rounded-md w-full text-zinc-600 dark:text-zinc-200 bg-zinc-50 dark:bg-zinc-700 hover:border-teal-500 focus:border-teal-500 dark:hover:border-teal-400 dark:focus:border-teal-400 focus:outline-none shadow-inner shadow-zinc-500 dark:shadow-black placeholder-zinc-400"
        />
        <label className="text-zinc-700 dark:text-zinc-200 ml-10 mr-2" htmlFor="endDate">
          End
        </label>
        <DatePicker
          selected={event.endDate ? new Date(event.endDate) : new Date(daySelected)}
          onChange={handleEndDateChange}
          showTimeSelect
          timeFormat="HH:mm"
          timeIntervals={15}
          dateFormat="MMMM d, yyyy h:mm aa"
          className="mb-2 p-2 focus:border hover:border rounded-md w-full text-zinc-600 dark:text-zinc-200 bg-zinc-50 dark:bg-zinc-700 hover:border-teal-500 focus:border-teal-500 dark:hover:border-teal-400 dark:focus:border-teal-400 focus:outline-none shadow-inner shadow-zinc-500 dark:shadow-black placeholder-zinc-400"
        />
        <label className="text-zinc-700 dark:text-zinc-200 ml-10 mr-2">Reoccurring</label>
        <select
          className="p-2 rounded-lg bg-zinc-200 shadow-sm shadow-zinc-400 dark:bg-zinc-700 text-zinc-600 dark:text-zinc-100 focus:outline-none dark:shadow-black"
          value={event.recurrencePattern}
          onChange={(e) => setEvent({ ...event, recurrencePattern: e.target.value })}>
          <option value="never">Never</option>
          <option value="daily">Daily</option>
          <option value="weekly">Weekly</option>
          <option value="monthly">Monthly</option>
        </select>
        {event.recurrencePattern === 'weekly' && (
          <div className="text-zinc-600 dark:text-zinc-300 mb-2">
            <p className="mb-2 mt-2 text-zinc-600 dark:text-zinc-300">Select Week Days:</p>
            {renderWeekDayCheckboxes()}
          </div>
        )}
        {event.recurrencePattern === 'monthly' && (
          <div className="text-zinc-600 dark:text-zinc-300 mb-2">
            <p className="mb-2 mt-2 text-zinc-600 dark:text-zinc-300">Select Month Days:</p>
            {renderMonthDayCheckboxes()}
          </div>
        )}
        <div className="mb-3"></div>
        <label className="text-zinc-700 dark:text-zinc-200" htmlFor="address">
          Address:
        </label>
        <input
          type="text"
          name="address"
          id="address"
          placeholder="Address"
          value={event.address}
          onChange={updateEvent('address')}
          className="mb-4 mt-1 p-2 focus:border hover:border rounded-md w-full text-zinc-600 dark:text-zinc-200 bg-zinc-50 dark:bg-zinc-700 hover:border-teal-500 focus:border-teal-500 dark:hover:border-teal-400 dark:focus:border-teal-400 focus:outline-none shadow-inner shadow-zinc-500 dark:shadow-black placeholder-zinc-400"
        />
        <label className="text-zinc-700 dark:text-zinc-200 mr-2" htmlFor="isPublic">
          Event type:
        </label>
        <select
          className="p-2 rounded-lg bg-zinc-200 shadow-sm shadow-zinc-400 dark:bg-zinc-700 text-zinc-600 dark:text-zinc-100 focus:outline-none dark:shadow-black"
          value={event.isPublic}
          onChange={updateEvent('isPublic')}>
          <option value="false">Private</option>
          <option value="true">Public</option>
        </select>
        <label className="text-zinc-700 dark:text-zinc-200 ml-10 mr-2">
          Category:
          <select
            className="ml-2 p-2 rounded-lg bg-zinc-200 shadow-sm shadow-zinc-400 dark:bg-zinc-700 text-zinc-600 dark:text-zinc-100 focus:outline-none dark:shadow-black"
            name="selectedCategory"
            id="category"
            value={event.category}
            onChange={updateEvent('category')}>
            <option value="party">Party</option>
            <option value="education">Education</option>
            <option value="work">Work</option>
            <option value="birthday">Birthday</option>
            <option value="holiday">Holiday</option>
          </select>
        </label>
        <div></div>
        <label htmlFor="participants"> </label>
        <button
          className="bg-cyan-500 dark:bg-cyan-400 hover:bg-zinc-100 dark:hover:bg-zinc-800 text-white dark:text-zinc-800 hover:text-cyan-500 dark:hover:text-cyan-400 rounded-full px-4 py-2.5 shadow-inner shadow-zinc-500 dark:shadow-black text-base font-bold mt-6"
          onClick={addParticipantsHandler}>
          + Participants
        </button>
        {isAddParticipantsOpen && (
          <AddEventParticipants
            eventId={event.id}
            addParticipants={setEventParticipants}
            eventParticipants={eventParticipants}
            onClose={setAddParticipantsOpen}
          />
        )}
        <div className="flex items-center space-x-2 mt-4">
          <button
            onClick={handleEventSubmit}
            className="flex items-center justify-center px-4 py-2 text-base font-medium leading-6 text-white whitespace-no-wrap bg-teal-500 border-2 border-transparent rounded-full shadow-sm hover:bg-transparent hover:text-teal-500 hover:border-teal-500 focus:outline-none">
            Create Event
          </button>
          <button
            onClick={onClose}
            className="flex items-center justify-center px-4 py-2 text-base font-medium leading-6 text-white whitespace-no-wrap bg-rose-400 border-2 border-transparent rounded-full shadow-sm hover:bg-transparent hover:text-rose-400 hover:border-rose-400 focus:outline-none">
            Cancel
          </button>
        </div>
      </div>
    </div>
  );
}
