import { useState, useEffect, useContext } from 'react';
import { getAllEvents, deleteEvent } from '../../services/eventService';
import { getUserData } from '../../services/userService';
import { auth } from '../../services/firebase';
import { Link } from 'react-router-dom';
import EventDetails from './EventDetails';
import CalendarContext from '../../context/CalendarContext';
import dayjs from 'dayjs';
import format from 'date-fns/format';

export default function EventsList() {
  const [, setCurrentUser] = useState();
  const [searchTerm, setSearchTerm] = useState('');
  const [, setIsDeleteModalOpen] = useState(false);
  const [isEventDetailsOpen, setIsEventDetailsOpen] = useState(false);
  const [filteredAndSortedEvents, setFilteredAndSortedEvents] = useState([]);
  const { setSelectedEvent } = useContext(CalendarContext);
  const [userHandle, setUserHandle] = useState('');
  const categories = ['work', 'education', 'party', 'birthday', 'holiday'];
  const [selectedCategories, setSelectedCategories] = useState(categories);

  useEffect(() => {
    getUserData(auth.currentUser.uid)
      .then((snapshot) => {
        if (snapshot.exists()) {
          const userData = snapshot.val();
          const handle = Object.values(userData)[0].handle;
          setCurrentUser(userData);
          setUserHandle(handle);
        }
      })
      .catch((error) => {
        console.error('Error fetching user data:', error);
      });
  }, []);

  const handleCategoryChange = (category) => {
    if (selectedCategories.includes(category)) {
      setSelectedCategories((prevCategories) =>
        prevCategories.filter((prevCategory) => prevCategory !== category)
      );
    } else {
      setSelectedCategories((prevCategories) => [...prevCategories, category]);
    }
  };

  const openDeleteModal = (event) => {
    setIsDeleteModalOpen(true);
    deleteEvent(event);
  };

  const eventDetailsHandler = () => {
    setIsEventDetailsOpen(true);
  };

  useEffect(() => {
    async function fetchEventData() {
      const data = await getAllEvents();
      const currentDate = new Date();
      const filteredAndSortedEvents = data
        .filter((event) => {
          if (selectedCategories.length === 0) {
            return true;
          }
          return (
            selectedCategories.includes(event.category) &&
            event.title.toLowerCase().includes(searchTerm.toLowerCase())
          );
        })
        .filter((event) => {
          const eventDate = new Date(event.startDate);
          return eventDate >= currentDate;
        })
        .sort((a, b) => {
          return new Date(a.startDate) - new Date(b.startDate);
        });
      setFilteredAndSortedEvents(filteredAndSortedEvents);
    }
    fetchEventData();
  }, [selectedCategories, searchTerm]);

  const categoryColors = {
    party: 'bg-violet-400',
    education: 'bg-emerald-400',
    work: 'bg-sky-400',
    birthday: 'bg-pink-300',
    holiday: 'bg-pink-500'
  };

  function getColorClass(category) {
    const colors = categoryColors;
    return colors[category] || 'bg-blue-200';
  }

  const handleInputChange = (e) => {
    const inputValue = e.target.value;
    setSearchTerm(inputValue);
  };

  return (
    <section className="dark:bg-zinc-800 dark:text-zinc-50 bg-zinc-200 text-zinc-800">
      <div className="container min-h-screen px-4 pb-12 pt-8 mx-auto">
        <div className="flex items-center p-2 mb-10 ml-2 w-1/3 rounded-full shadow shadow-zinc-400 dark:shadow-black focus-within:shadow-cyan-600 dark:focus-within:shadow-cyan-600 bg-zinc-100 dark:bg-zinc-900 overflow-hidden">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-5 w-5 text-zinc-500 dark:text-zinc-200"
            viewBox="0 0 20 20"
            fill="currentColor">
            <path
              fillRule="evenodd"
              d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
              clipRule="evenodd"
            />
          </svg>
          <input
            className="outline-none ml-1 block bg-zinc-100 dark:bg-zinc-900 text-zinc-700 dark:text-zinc-300 dark:placeholder-zinc-500 placeholder-zinc-500"
            type="text"
            placeholder="Search event..."
            value={searchTerm}
            onChange={handleInputChange}
          />
        </div>
        <div className="grid gap-4 mx-4 sm:grid-cols-12">
          <div className="col-span-12 sm:col-span-3">
            <div className="text-center sm:text-left mb-14 before:block before:w-24 before:h-3 before:mb-5 before:rounded-md before:mx-auto sm:before:mx-0 before:bg-cyan-500 dark:before:bg-cyan-400">
              <h3 className="text-3xl font-semibold">Events</h3>
              <span className="text-sm font-bold uppercase text-zinc-600 dark:text-zinc-300">
                All upcoming events in your schedule
              </span>
            </div>
            <div className="mb-4">
              <h4 className="text-lg font-semibold mb-2 text-zinc-700 dark:text-zinc-200">
                Labels:
              </h4>
              {categories.map((category) => (
                <div key={category} className="flex items-center mb-2">
                  <input
                    type="checkbox"
                    id={category}
                    checked={selectedCategories.includes(category)}
                    onChange={() => handleCategoryChange(category)}
                    className="h-6 w-6 accent-cyan-500 mr-2"
                  />
                  <label
                    htmlFor={category}
                    className={`font-semibold text-white dark:text-black p-0.5 rounded ${getColorClass(
                      category
                    )}`}>
                    {category}
                  </label>
                </div>
              ))}
            </div>
          </div>
          <div className="relative col-span-10 px-4 space-y-6 sm:col-span-9">
            <div className="col-span-12 space-y-12 relative px-4 sm:col-span-8 sm:space-y-8 sm:before:absolute sm:before:top-2 sm:before:bottom-0 sm:before:w-0.5 sm:before:-left-3 before:bg-zinc-300 before:dark:bg-zinc-800">
              <div>
                {filteredAndSortedEvents.map((event) => (
                  <span
                    key={event.id}
                    className="lg:flex shadow rounded-md bg-zinc-100 shadow-zinc-400 dark:bg-zinc-900 dark:shadow-inner dark:shadow-black mb-2">
                    {isEventDetailsOpen && <EventDetails eventId={event.id} />}
                    <div
                      className={`${
                        categoryColors[event.category]
                      } rounded-md lg:w-2/12 py-4 block h-full shadow-inner`}>
                      <div className="text-center tracking-wide">
                        <div className="text-zinc-50 dark:text-zinc-900 font-bold text-4xl ">
                          {dayjs(event.startDate).date()}
                        </div>
                        <div className="text-zinc-50 dark:text-zinc-900 font-normal text-2xl">
                          {dayjs(event.startDate).format('MMM')}
                        </div>
                      </div>
                    </div>
                    <div className="w-full  lg:w-11/12 xl:w-full px-1 py-5 lg:px-2 lg:py-2 tracking-wide">
                      <div className="flex flex-row lg:justify-start justify-center mb-1 mt-1.5">
                        <div className="text-zinc-700 dark:text-zinc-300 font-medium text-sm text-center lg:text-left">
                          <div className="flex items-center">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              fill="none"
                              viewBox="0 0 24 24"
                              strokeWidth={1.5}
                              stroke="currentColor"
                              className="w-4 h-4 ml-1 mr-1 text-teal-500 dark:text-teal-400">
                              <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                d="M12 6v6h4.5m4.5 0a9 9 0 11-18 0 9 9 0 0118 0z"
                              />
                            </svg>
                            <span className="text-grey-500">
                              {event.startDate && format(new Date(event.startDate), 'HH:mm')} -{' '}
                              {event.endDate && format(new Date(event.endDate), 'HH:mm')}
                            </span>
                          </div>
                        </div>
                        <div className="flex items-center">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            strokeWidth={1.5}
                            stroke="currentColor"
                            className="w-4 h-4 ml-4 mr-1 text-amber-500 dark:text-amber-400">
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              d="M17.982 18.725A7.488 7.488 0 0012 15.75a7.488 7.488 0 00-5.982 2.975m11.963 0a9 9 0 10-11.963 0m11.963 0A8.966 8.966 0 0112 21a8.966 8.966 0 01-5.982-2.275M15 9.75a3 3 0 11-6 0 3 3 0 016 0z"
                            />
                          </svg>
                          <span className="text-zinc-700 dark:text-zinc-300 font-medium text-sm text-center lg:text-left">
                            Organized by: {event.author}
                          </span>
                        </div>
                      </div>
                      <div className="font-semibold text-zinc-800 dark:text-zinc-100 text-xl text-center lg:text-left px-2">
                        {event.title}
                      </div>
                      <div className="flex items-center">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                          strokeWidth={1.5}
                          stroke="currentColor"
                          className="w-4 h-4 ml-1 mr-1 text-cyan-500 dark:text-cyan-400">
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M15 10.5a3 3 0 11-6 0 3 3 0 016 0z"
                          />
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M19.5 10.5c0 7.142-7.5 11.25-7.5 11.25S4.5 17.642 4.5 10.5a7.5 7.5 0 1115 0z"
                          />
                        </svg>
                        <div className="text-zinc-700 dark:text-zinc-300 font-medium text-sm pt-1 text-center lg:text-left">
                          Address: {event.address}
                        </div>
                      </div>
                    </div>
                    <div></div>
                    <div>
                      <Link
                        to={`/event-details/${event.id}`}
                        key={event.id}
                        onClick={() => {
                          eventDetailsHandler();
                          setSelectedEvent(event);
                        }}
                        className="flex items-center justify-center">
                        <div className="mb-2 mt-3">
                          <button className="bg-teal-400 dark:bg-teal-400 hover:bg-zinc-100 dark:hover:bg-zinc-900 text-white dark:text-zinc-900 hover:text-teal-400 dark:hover:text-teal-400 rounded-full px-4 py-2 hover:border hover:border-teal-400 shadow-zinc-500 dark:shadow-black text-sm font-bold mr-2">
                            Details
                          </button>
                        </div>
                      </Link>
                      {userHandle === event.author && (
                        <div className="mb-2 ml-1">
                          <button
                            className="bg-rose-400 dark:bg-rose-400 hover:bg-zinc-100 dark:hover:bg-zinc-900 text-white dark:text-zinc-900 hover:text-rose-400 dark:hover:text-rose-400 rounded-full px-4 py-2 hover:border hover:border-rose-400 shadow-zinc-500 dark:shadow-black text-sm font-bold mr-2"
                            onClick={() => openDeleteModal(event.id)}>
                            Delete
                          </button>
                        </div>
                      )}
                    </div>
                  </span>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
