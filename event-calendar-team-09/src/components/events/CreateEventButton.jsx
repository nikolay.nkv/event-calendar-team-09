import { useState } from 'react';
import EventModal from './EventModal';

export default function CreateEventButton() {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const openModal = () => {
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };

  return (
    <div className="eventButton">
      <button
        onClick={openModal}
        className="bg-teal-500 dark:bg-teal-400 hover:bg-zinc-100 dark:hover:bg-zinc-800 text-white dark:text-zinc-800 hover:text-teal-500 dark:hover:text-teal-400 rounded-full px-4 py-2.5 shadow-inner shadow-zinc-500 dark:shadow-black text-base font-bold">
        <span>+ Add Event</span>
      </button>
      {isModalOpen && <EventModal onClose={closeModal} />}
    </div>
  );
}
