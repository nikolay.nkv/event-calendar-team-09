import { useState, useEffect } from 'react';
import Geocode from 'react-geocode';

function EventAddress() {
  const [userLocation, setUserLocation] = useState(null);
  Geocode.setApiKey('');

  useEffect(() => {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        const { latitude, longitude } = position.coords;
        Geocode.fromLatLng(latitude, longitude).then(
          (response) => {
            const address = response.results[0].formatted_address;
            setUserLocation(address);
          },
          (error) => {
            console.error('Error fetching address:', error);
          }
        );
      },
      (error) => {
        console.error('Error getting user location:', error);
      }
    );
  }, []);

  return (
    <div>
      <h2>User Location</h2>
      {userLocation ? <p>User's Address: {userLocation}</p> : <p>Loading user location...</p>}
    </div>
  );
}

export default EventAddress;
