import { useContext, useState, useEffect } from 'react';
import {
  changeAddress,
  changeCategory,
  changeDescription,
  changeEndDate,
  changeRecurrencePattern,
  changeStartDate,
  changeTitle,
  changeSelectedMounthDays,
  changeSelectedWeekDays
} from '../../services/eventService';
import CalendarContext from '../../context/CalendarContext';
import DatePicker from 'react-datepicker';
import format from 'date-fns/format';
import { getUserData } from '../../services/userService';
import { auth } from '../../services/firebase';

export default function EventDetails() {
  const [, setCurrentUser] = useState();
  const [editMode, setEditMode] = useState(false);
  const [, setEvent] = useState(null);
  const [titleInput, setTitleInput] = useState('');
  const [descriptionInput, setDescriptionInput] = useState('');
  const [startDateInput, setStartDateInput] = useState('');
  const [endDateInput, setEndDateInput] = useState('');
  const [reoccurring, setReoccurring] = useState('never');
  const [addressInput, setAddressInput] = useState('');
  const [category, setCategory] = useState('');
  const { selectedEvent, setSelectedEvent, daySelected } = useContext(CalendarContext);
  const [selectedMonthDays, setSelectedMonthDays] = useState([]);
  const [selectedWeekdays, setSelectedWeekdays] = useState([]);
  const [userHandle, setUserHandle] = useState('');
  const [showAlert, setShowAlert] = useState(false);

  useEffect(() => {
    getUserData(auth.currentUser.uid)
      .then((snapshot) => {
        if (snapshot.exists()) {
          const userData = snapshot.val();
          const handle = Object.values(userData)[0].handle;
          setCurrentUser(userData);
          setUserHandle(handle);
        }
      })
      .catch((error) => {
        console.error('Error fetching user data:', error);
      });
  }, []);

  const handleEditClick = () => {
    if (userHandle !== selectedEvent.author) {
      setShowAlert(true);
      return;
    }
    setEditMode(true);
    setTitleInput(selectedEvent.title || '');
    setDescriptionInput(selectedEvent.description || '');
    setStartDateInput(selectedEvent.startDate || '');
    setEndDateInput(selectedEvent.endDate || '');
    setReoccurring(selectedEvent.reoccurring || '');
    setAddressInput(selectedEvent.address || '');
    setCategory(selectedEvent.category || '');
    setSelectedMonthDays(selectedEvent.selectedMonthDays || []);
    setSelectedWeekdays(selectedEvent.selectedWeekdays || []);
  };

  const handleCancelClick = () => {
    setEditMode(false);
  };

  const closeAlert = () => {
    setShowAlert(false);
  };

  const handleSaveClick = async () => {
    try {
      await Promise.all([
        changeTitle(selectedEvent.id, titleInput),
        changeDescription(selectedEvent.id, descriptionInput),
        changeStartDate(selectedEvent.id, startDateInput),
        changeEndDate(selectedEvent.id, endDateInput),
        changeRecurrencePattern(selectedEvent.id, reoccurring),
        changeAddress(selectedEvent.id, addressInput),
        changeCategory(selectedEvent.id, category),
        changeSelectedMounthDays(selectedEvent.id, selectedMonthDays),
        changeSelectedWeekDays(selectedEvent.id, selectedWeekdays)
      ]);

      setEvent((prevEvent) => ({
        ...prevEvent,
        title: titleInput,
        description: descriptionInput,
        startDate: startDateInput,
        endDate: endDateInput,
        reoccurring: reoccurring,
        address: addressInput,
        category: category,
        selectedMonthDays: selectedMonthDays,
        selectedWeekdays: selectedWeekdays
      }));

      setEditMode(false);
    } catch (error) {
      console.error('Error updating event:', error);
    }
  };

  const handleStartDateChange = (date) => {
    setSelectedEvent({
      ...selectedEvent,
      startDate: date
    });
    setStartDateInput(date);
  };

  const handleEndDateChange = (date) => {
    setSelectedEvent({
      ...selectedEvent,
      endDate: date
    });
  };

  const categoryImages = {
    party: 'https://getout.events/wp-content/uploads/2021/11/event-organizer-1.png',
    education:
      'https://cdn.uniacco.com/blog/wp-content/uploads/2021/06/02122221/stem-list-EVgsAbL51Rk-unsplash1-min-1024x576.jpg',
    work: 'https://images.businessnewsdaily.com/app/uploads/2022/04/04080243/Employee_Props_Getty_fizkes.jpg',
    birthday:
      'https://chicagoparent.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/trendy-birthday-party-themes-for-kids-photo-credit-istock-ruthblack.jpg',
    holiday:
      'https://ichef.bbci.co.uk/news/976/cpsprodpb/3981/production/_122612741_gettyimages-672425798.jpg'
  };

  return (
    <>
      <div className="min-h-screen bg-zinc-200 dark:bg-zinc-900">
        {showAlert && (
          <div
            className="bg-rose-100 dark:bg-rose-300 border border-rose-400 dark:border-rose-600 text-rose-700 dark:text-rose-800 px-4 py-3 rounded relative"
            role="alert">
            <strong className="font-bold">Access Denied! </strong>
            <span className="block sm:inline">
              You are not the author of this event and can not edit it.
            </span>
            <span
              className="absolute top-0 bottom-0 right-0 px-4 py-3 cursor-pointer"
              onClick={closeAlert}>
              <svg
                className="fill-current h-6 w-6 text-rose-500 dark:text-rose-600"
                role="button"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20">
                <title>Close</title>
                <path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z" />
              </svg>
            </span>
          </div>
        )}
        <div className="flex items-start pt-16">
          <div className="w-full ml-10 mr-10 flex flex-row items-center justify-center bg-zinc-50 dark:bg-zinc-800 dark:shadow-inner dark:shadow-black rounded-lg shadow-md shadow-zinc-400">
            <div className="w-full flex flex-col items-center justify-center">
              <figure className="w-1/3 rounded-md md:w-full overflow-hidden pr-10">
                <img
                  src={
                    categoryImages[selectedEvent.category] ||
                    'https://www.freepik.com/free-vector/festive-calendar-event-holiday-celebration-party-work-schedule-planning-project-management-deadline-idea-office-managers-excited-colleagues_11667054.htm#query=event%20illustration&position=0&from_view=keyword&track=ais'
                  }
                  alt="category image"
                />
              </figure>
            </div>
            <div className="w-full md:w-4/5 flex flex-col pr-10">
              <div className="items-end justify-end flex space-x-2">
                {editMode ? (
                  <div
                    className="w-8 h-8 mt-4 cursor-pointer text-zinc-700 hover:text-teal-500 dark:text-zinc-300 dark:hover:text-teal-400"
                    onClick={handleSaveClick}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="currentColor">
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
                      />
                    </svg>
                  </div>
                ) : (
                  <div
                    className="w-6 h-6 cursor-pointer text-zinc-700 hover:text-cyan-500 dark:text-zinc-300 dark:hover:text-cyan-400"
                    onClick={handleEditClick}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="currentColor">
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10"
                      />
                    </svg>
                  </div>
                )}
                {editMode && (
                  <div
                    className="w-8 h-8 mt-4 cursor-pointer text-zinc-700 hover:text-rose- dark:text-zinc-300 dark:hover:text-rose-400"
                    onClick={handleCancelClick}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="currentColor">
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M9.75 9.75l4.5 4.5m0-4.5l-4.5 4.5M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
                      />
                    </svg>
                  </div>
                )}
              </div>
              <div className="flex flex-col items-start">
                <h1 className="text-left text-2xl font-bold mb-4 text-zinc-800 dark:text-zinc-100">
                  {' '}
                  {editMode ? (
                    <input
                      type="text"
                      value={titleInput}
                      onChange={(e) => setTitleInput(e.target.value)}
                      className="shadow-inner shadow-zinc-600 bg-zinc-200 dark:bg-zinc-600 dark:shadow-black rounded-lg p-2 w-full text-zinc-600 dark:text-zinc-200"
                    />
                  ) : (
                    <span className="text-grey-500">{selectedEvent.title}</span>
                  )}
                </h1>
                <ul>
                  <li className="text-sm text-zinc-700 dark:text-zinc-200 flex items-center mb-1">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="currentColor"
                      className="w-6 h-6 mr-2 text-amber-500 dark:text-amber-400">
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M17.982 18.725A7.488 7.488 0 0012 15.75a7.488 7.488 0 00-5.982 2.975m11.963 0a9 9 0 10-11.963 0m11.963 0A8.966 8.966 0 0112 21a8.966 8.966 0 01-5.982-2.275M15 9.75a3 3 0 11-6 0 3 3 0 016 0z"
                      />
                    </svg>
                    <span className="text-grey-500">Created by: {selectedEvent.author}</span>
                  </li>
                  <li className="text-sm text-zinc-700 dark:text-zinc-200 flex items-center mb-1">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="currentColor"
                      className="w-6 h-6 mr-2 text-cyan-500 dark:text-cyan-400">
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M15 10.5a3 3 0 11-6 0 3 3 0 016 0z"
                      />
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M19.5 10.5c0 7.142-7.5 11.25-7.5 11.25S4.5 17.642 4.5 10.5a7.5 7.5 0 1115 0z"
                      />
                    </svg>
                    {editMode ? (
                      <input
                        type="text"
                        value={addressInput}
                        onChange={(e) => setAddressInput(e.target.value)}
                        className="shadow-inner shadow-zinc-600 bg-zinc-200 dark:bg-zinc-600 dark:shadow-black rounded-lg p-2 w-full text-zinc-600 dark:text-zinc-200"
                      />
                    ) : (
                      <span className="text-grey-500">{selectedEvent.address}</span>
                    )}
                  </li>
                  <li className="text-sm text-zinc-700 dark:text-zinc-200 flex items-center">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="currentColor"
                      className="w-6 h-6 mr-2 text-teal-500 dark:text-teal-400">
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M12 6v6h4.5m4.5 0a9 9 0 11-18 0 9 9 0 0118 0z"
                      />
                    </svg>
                    {editMode ? (
                      <>
                        <DatePicker
                          selected={
                            selectedEvent.startDate
                              ? new Date(selectedEvent.startDate)
                              : new Date(daySelected)
                          }
                          onChange={handleStartDateChange}
                          showTimeSelect
                          timeFormat="HH:mm"
                          timeIntervals={15}
                          dateFormat="dd MMMM yyyy, HH:mm"
                          className="shadow-inner shadow-zinc-600 bg-zinc-200 dark:bg-zinc-600 dark:shadow-black rounded-lg p-2 w-full text-zinc-600 dark:text-zinc-200"
                        />
                        <span className="text-grey-600 mx-2">-</span>
                        <DatePicker
                          selected={
                            selectedEvent.endDate
                              ? new Date(selectedEvent.endDate)
                              : new Date(daySelected)
                          }
                          onChange={handleEndDateChange}
                          showTimeSelect
                          timeFormat="HH:mm"
                          timeIntervals={15}
                          dateFormat="dd MMMM yyyy, HH:mm"
                          className="shadow-inner shadow-zinc-600 bg-zinc-200 dark:bg-zinc-600 dark:shadow-black rounded-lg p-2 w-full text-zinc-600 dark:text-zinc-200"
                        />
                      </>
                    ) : (
                      <span className="text-grey-500">
                        {selectedEvent.startDate &&
                          format(new Date(selectedEvent.startDate), 'dd MMMM yyyy, HH:mm')}{' '}
                        -{' '}
                        {selectedEvent.endDate &&
                          format(new Date(selectedEvent.endDate), 'dd MMMM yyyy, HH:mm')}
                      </span>
                    )}
                  </li>
                </ul>
                <p className="text-left text-zinc-700 mt-4 border-t pt-2 border-b dark:text-zinc-300 dark:border-zinc-600 pb-2 mb-4 font-normal leading-6 text-base">
                  {editMode ? (
                    <textarea
                      value={descriptionInput}
                      onChange={(e) => setDescriptionInput(e.target.value)}
                      className="shadow-inner shadow-zinc-600 bg-zinc-200 dark:bg-zinc-600 dark:shadow-black rounded-lg p-2 w-full text-zinc-600 dark:text-zinc-200"
                    />
                  ) : (
                    <span className="text-grey-500">{selectedEvent.description}</span>
                  )}
                </p>
                <ul>
                  <li className="flex py-2">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="currentColor"
                      className="w-6 h-6 mr-2 text-violet-500 dark:text-violet-400">
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M18 18.72a9.094 9.094 0 003.741-.479 3 3 0 00-4.682-2.72m.94 3.198l.001.031c0 .225-.012.447-.037.666A11.944 11.944 0 0112 21c-2.17 0-4.207-.576-5.963-1.584A6.062 6.062 0 016 18.719m12 0a5.971 5.971 0 00-.941-3.197m0 0A5.995 5.995 0 0012 12.75a5.995 5.995 0 00-5.058 2.772m0 0a3 3 0 00-4.681 2.72 8.986 8.986 0 003.74.477m.94-3.197a5.971 5.971 0 00-.94 3.197M15 6.75a3 3 0 11-6 0 3 3 0 016 0zm6 3a2.25 2.25 0 11-4.5 0 2.25 2.25 0 014.5 0zm-13.5 0a2.25 2.25 0 11-4.5 0 2.25 2.25 0 014.5 0z"
                      />
                    </svg>
                    <span className="font-bold w-24 text-zinc-700 dark:text-zinc-300">
                      Participants:
                    </span>
                    <span className="text-zinc-600 dark:text-zinc-400">
                      {Array.isArray(selectedEvent.participants)
                        ? selectedEvent.participants.join(', ')
                        : selectedEvent.participants}
                    </span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
