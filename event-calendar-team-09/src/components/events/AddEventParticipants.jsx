import { useState, useEffect } from 'react';
import { getUserData, getUserByHandle } from '../../services/userService';
import { auth } from '../../services/firebase';
import { storage } from '../../services/firebase';
import { getDownloadURL, ref } from 'firebase/storage';
import PropTypes from 'prop-types';

function AddEventParticipants({ addParticipants, eventParticipants, onClose }) {
  const [selectedParticipants, setSelectedParticipants] = useState([]);
  const [contactsListArray, setContactsListArray] = useState([]);
  const [, setProfilePhotoUrls] = useState({});
  const defaultPlaceholderUrl =
    'https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png';
  const [userProfile, setUserProfile] = useState(null);

  async function fetchUserProfile(handle) {
    try {
      const userProfileSnapshot = await getUserByHandle(handle);
      const userProfileData = userProfileSnapshot.val();
      return userProfileData;
    } catch (error) {
      console.error('Error fetching user profile:', error);
      return null;
    }
  }

  useEffect(() => {
    async function fetchUserProfileData() {
      if (auth.currentUser) {
        const fetchedUserProfile = await fetchUserProfile(auth.currentUser.handle);
        setUserProfile(fetchedUserProfile);
      }
    }

    fetchUserProfileData();
  }, []);

  useEffect(() => {
    if (auth.currentUser) {
      getUserData(auth.currentUser.uid)
        .then((snapshot) => {
          if (snapshot.exists()) {
            const userData = snapshot.val();
            const firstUser = Object.values(userData)[0];
            setUserProfile(firstUser);
          }
        })
        .catch((error) => {
          console.error('Error fetching user data:', error);
        });
    }
  }, []);

  useEffect(() => {
    if (userProfile) {
      const userContacts = userProfile.contacts || {};
      const contactHandles = Object.keys(userContacts);
      const contactsHandles = [];

      const fetchContactData = async () => {
        const contactDataArray = await Promise.all(
          contactHandles.map(async (contactHandle) => {
            const contactDataSnapshot = await getUserByHandle(contactHandle);

            if (contactDataSnapshot.exists()) {
              const contactData = contactDataSnapshot.val();
              const profilePhotoUrl = contactData.profilePhotoUrl || defaultPlaceholderUrl;
              contactsHandles.push(contactData);
              if (contactData.uid) {
                try {
                  const userPhotoRef = ref(storage, `${contactData.uid}.png`);
                  const url = await getDownloadURL(userPhotoRef);

                  return {
                    ...contactData,
                    profilePhotoUrl: url
                  };
                } catch (error) {
                  console.error(`Error fetching profile photo for ${contactData.uid}:`, error);
                }
              }

              return {
                ...contactData,
                profilePhotoUrl
              };
            }
            return null;
          })
        );
        setContactsListArray(contactsHandles);

        const filteredContactDataArray = contactDataArray.filter((data) => data !== null);

        const profilePhotoUrlMap = filteredContactDataArray.reduce((acc, contactData) => {
          if (contactData.uid && contactData.profilePhotoUrl) {
            acc[contactData.uid] = contactData.profilePhotoUrl;
          }
          return acc;
        }, {});
        setProfilePhotoUrls(profilePhotoUrlMap);
      };

      fetchContactData();
    }
  }, [userProfile]);

  const addParticipantToEventsParticipantsData = (selectedParticipants) => {
    addParticipants([...eventParticipants, ...selectedParticipants]);

    onClose(false);
  };

  const toggleParticipantSelection = (participant) => {
    if (selectedParticipants.includes(participant)) {
      setSelectedParticipants(selectedParticipants.filter((c) => c !== participant));
    } else {
      setSelectedParticipants([...selectedParticipants, participant]);
    }
  };

  AddEventParticipants.propTypes = {
    addParticipants: PropTypes.func.isRequired,
    eventParticipants: PropTypes.array.isRequired,
    onClose: PropTypes.func.isRequired
  };

  return (
    <div className="fixed inset-0 flex items-center justify-center z-50">
      <div className="bg-zinc-200 dark:bg-zinc-900 dark:shadow-black p-6 rounded-lg shadow-md shadow-zinc-600 w-96">
        <h2 className="text-xl text-zinc-700 dark:text-zinc-200 font-semibold mb-4">
          Select Participant From Contacts List
        </h2>
        <div className="h-80 overflow-y-scroll">
          <ul className="space-y-1">
            {contactsListArray.map((participant) => (
              <li key={participant.uid} className="flex justify-between items-center p-2 rounded">
                <span className="text-zinc-700 dark:text-zinc-200">
                  {participant.firstName} {participant.lastName}
                </span>
                <button
                  onClick={() => toggleParticipantSelection(participant.handle)}
                  className={`px-2 py-1 rounded-full ${
                    selectedParticipants.includes(participant.handle)
                      ? 'bg-rose-500 dark:bg-rose-400 text-white dark:text-black font-semibold'
                      : 'bg-teal-500 dark:bg-teal-400 text-white dark:text-black font-semibold'
                  }`}>
                  {selectedParticipants.includes(participant.handle) ? '- Remove' : '+ Add'}
                </button>
              </li>
            ))}
          </ul>
        </div>
        <button
          className="bg-cyan-500 dark:bg-cyan-400 hover:bg-zinc-100 dark:hover:bg-zinc-800 text-white dark:text-zinc-900 hover:text-cyan-500 dark:hover:text-cyan-400 rounded-full px-4 py-2.5 shadow-inner shadow-zinc-500 dark:shadow-black text-base font-bold mt-6"
          onClick={() => {
            addParticipantToEventsParticipantsData(selectedParticipants);
          }}>
          + Add Selected Participant
        </button>
        <div></div>
        <button
          className="bg-rose-500 dark:bg-rose-400 hover:bg-zinc-100 dark:hover:bg-zinc-800 text-white dark:text-zinc-900 hover:text-rose-500 dark:hover:text-rose-400 rounded-full px-4 py-2.5 shadow-inner shadow-zinc-500 dark:shadow-black text-base font-bold mt-2"
          onClick={() => {
            onClose(false);
          }}>
          Cancel
        </button>
      </div>
    </div>
  );
}

export default AddEventParticipants;
