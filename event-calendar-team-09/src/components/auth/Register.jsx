import { useContext, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { AuthContext } from '../../context/AuthContext';
import { getUserByHandle, createUserHandle } from '../../services/userService';
import { registerUser } from '../../services/authService';
import { Link } from 'react-router-dom';
import {
  MIN_NAME_LENGTH,
  MAX_NAME_LENGTH,
  MIN_PASSWORD_LENGTH,
  MAX_PASSWORD_LENGTH
} from '../../utils/constants';

export default function Register() {
  const [form, setForm] = useState({
    email: '',
    password: '',
    handle: '',
    firstName: '',
    lastName: '',
    phoneNumber: ''
  });

  const { setUser } = useContext(AuthContext);
  const navigate = useNavigate();
  const [errorMessage, setErrorMessage] = useState('');

  const updateForm = (prop) => (e) => {
    setForm({
      ...form,
      [prop]: e.target.value
    });
  };

  const onRegister = () => {
    if (
      !form.firstName ||
      form.firstName.length < MIN_NAME_LENGTH ||
      form.firstName.length > MAX_NAME_LENGTH ||
      !/^[A-Za-z]{1,30}$/.test(form.firstName)
    ) {
      setErrorMessage(
        'First name must be between 1 and 30 characters and must contain only uppercase and lowercase letters.'
      );
      return;
    }

    if (
      !form.lastName ||
      form.lastName.length < MIN_NAME_LENGTH ||
      form.lastName.length > MAX_NAME_LENGTH ||
      !/^[A-Za-z]{1,30}$/.test(form.lastName)
    ) {
      setErrorMessage(
        'Last name must be between 1 and 30 characters and must contain only uppercase and lowercase letters'
      );
      return;
    }

    if (!form.email) {
      setErrorMessage('Email is required');
      return;
    }

    if (!form.phoneNumber || !/^[0-9]{10}$/.test(form.phoneNumber)) {
      setErrorMessage('Please enter a valid 10-digit phone number');
      return;
    }

    if (
      !form.password ||
      form.password.length < MIN_PASSWORD_LENGTH ||
      form.password.length > MAX_PASSWORD_LENGTH
    ) {
      setErrorMessage('Password is required and must be between 8 and 30 characters');
      return;
    }

    if (!form.handle) {
      setErrorMessage('User name is required');
      return;
    }

    getUserByHandle(form.handle)
      .then((snapshot) => {
        if (snapshot.exists()) {
          setErrorMessage('User name already exists');
          return;
        }

        return registerUser(form.email, form.password);
      })
      .then((credential) => {
        return createUserHandle(
          form.handle,
          credential.user.uid,
          form.email,
          form.firstName,
          form.lastName,
          form.phoneNumber
        ).then(() => {
          setUser({
            user: credential.user
          });
        });
      })
      .then(() => {
        navigate('/home');
      })
      .catch((error) => {
        console.error('Error during registration:', error);
      });
  };

  return (
    <section className="bg-white dark:bg-zinc-950">
      <div className="flex justify-center min-h-screen">
        <div
          className="hidden bg-cover lg:block lg:w-2/5"
          style={{
            backgroundImage:
              "url('https://i.pinimg.com/564x/5f/f9/2c/5ff92c5a520e1568564caf6f6fbee188.jpg')"
          }}></div>
        <div className="flex items-center w-full max-w-3xl p-8 mx-auto lg:px-12 lg:w-3/5">
          <div className="w-full">
            <h1 className="text-2xl font-semibold tracking-wider text-zinc-800 capitalize dark:text-zinc-50">
              Create your account.
            </h1>
            <p className="mt-4 text-zinc-500 dark:text-zinc-400">For a better-planned future.</p>
            <div className="mt-6">
              <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
                <div className="grid grid-cols-2 gap-8 mb-8">
                  <div>
                    <label
                      htmlFor="firstName"
                      className="block mb-2 text-sm font-medium text-zinc-900 dark:text-white">
                      First name
                    </label>
                    <input
                      type="text"
                      name="firstName"
                      id="firstName"
                      value={form.firstName}
                      onChange={updateForm('firstName')}
                      className="bg-zinc-50 border border-zinc-300 text-zinc-900 sm:text-sm rounded-lg focus:ring-zinc-600 focus:border-zinc-600 block w-full p-2.5 dark:bg-zinc-700 dark:border-zinc-600 dark:placeholder-zinc-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                      placeholder="First name"
                    />
                  </div>
                  <div>
                    <label
                      htmlFor="lastName"
                      className="block mb-2 text-sm font-medium text-zinc-900 dark:text-white">
                      Last name
                    </label>
                    <input
                      type="text"
                      name="lastName"
                      id="lastName"
                      value={form.lastName}
                      onChange={updateForm('lastName')}
                      className="bg-zinc-50 border border-zinc-300 text-zinc-900 sm:text-sm rounded-lg focus:ring-zinc-600 focus:border-zinc-600 block w-full p-2.5 dark:bg-zinc-700 dark:border-zinc-600 dark:placeholder-zinc-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                      placeholder="Last name"
                    />
                  </div>
                  <div>
                    <label
                      htmlFor="handle"
                      className="block mb-2 text-sm font-medium text-zinc-900 dark:text-white">
                      User name
                    </label>
                    <input
                      type="text"
                      name="handle"
                      id="handle"
                      value={form.handle}
                      onChange={updateForm('handle')}
                      className="bg-zinc-50 border border-zinc-300 text-zinc-900 sm:text-sm rounded-lg focus:ring-zinc-600 focus:border-zinc-600 block w-full p-2.5 dark:bg-zinc-700 dark:border-zinc-600 dark:placeholder-zinc-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                      placeholder="User name"
                    />
                  </div>
                  <div>
                    <label
                      htmlFor="email"
                      className="block mb-2 text-sm font-medium text-zinc-900 dark:text-white">
                      E-mail
                    </label>
                    <input
                      type="email"
                      name="email"
                      id="email"
                      value={form.email}
                      onChange={updateForm('email')}
                      className="bg-zinc-50 border border-zinc-300 text-zinc-900 sm:text-sm rounded-lg focus:ring-zinc-600 focus:border-zinc-600 block w-full p-2.5 dark:bg-zinc-700 dark:border-zinc-600 dark:placeholder-zinc-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                      placeholder="name@mail.com"
                    />
                  </div>
                  <div>
                    <label
                      htmlFor="phoneNumber"
                      className="block mb-2 text-sm font-medium text-zinc-900 dark:text-white">
                      Phone Number
                    </label>
                    <input
                      type="tel"
                      name="phoneNumber"
                      id="phoneNumber"
                      value={form.phoneNumber}
                      onChange={updateForm('phoneNumber')}
                      pattern="[0-9]{10}"
                      className="bg-zinc-50 border border-zinc-300 text-zinc-900 sm:text-sm rounded-lg focus:ring-zinc-600 focus:border-zinc-600 block w-full p-2.5 dark:bg-zinc-700 dark:border-zinc-600 dark:placeholder-zinc-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                      placeholder="+359"
                    />
                  </div>

                  <div>
                    <label
                      htmlFor="password"
                      className="block mb-2 text-sm font-medium text-zinc-900 dark:text-white">
                      Password
                    </label>
                    <input
                      type="password"
                      name="password"
                      id="password"
                      value={form.password}
                      onChange={updateForm('password')}
                      placeholder="••••••••"
                      className="bg-zinc-50 border border-zinc-300 text-zinc-900 sm:text-sm rounded-lg focus:ring-zinc-600 focus:border-zinc-600 block w-full p-2.5 dark:bg-zinc-700 dark:border-zinc-600 dark:placeholder-zinc-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    />
                  </div>
                </div>
                <div className="flex items-start">
                  <div className="flex items-center h-5">
                    <input
                      id="terms"
                      aria-describedby="terms"
                      type="checkbox"
                      className="w-4 h-4 border border-zinc-300 rounded bg-zinc-50 focus:ring-3 focus:ring-zinc-300 dark:bg-zinc-700 dark:border-zinc-600 dark:focus:ring-zinc-600 dark:ring-offset-zinc-800"
                    />
                  </div>
                  <div className="ml-3 text-sm">
                    <label htmlFor="terms" className="font-light text-zinc-500 dark:text-zinc-300">
                      I accept the{' '}
                      <a
                        className="font-medium text-zinc-600 hover:underline dark:text-zinc-500"
                        href="#">
                        Terms and Conditions
                      </a>
                    </label>
                  </div>
                </div>
                {errorMessage && <p className="error-message">{errorMessage}</p>}
                <div className="flex justify-center">
                  <button
                    type="submit"
                    onClick={onRegister}
                    className="w-full text-teal-500 bg-white hover:bg-teal-500 border-2 border-teal-500 hover:text-white dark:bg-zinc-950 dark:hover:bg-teal-400 dark:border-teal-400 dark:text-teal-400 dark:hover:text-white hover:font-bold focus:outline-none font-medium rounded-lg text-xl px-5 py-2.5 text-center">
                    Create an account
                  </button>
                </div>
                <p className="text-center text-lg text-zinc-600 dark:text-zinc-300">
                  Already have an account?{' '}
                  <Link
                    to="/log-in"
                    href="#"
                    className="font-medium text-teal-500 underline-offset-4 hover:underline">
                    Log in here
                  </Link>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
