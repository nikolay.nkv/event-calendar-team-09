import { useState, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import { AuthContext } from '../../context/AuthContext';
import { loginUser } from '../../services/authService';
import { Link } from 'react-router-dom';

export default function Login() {
  const [form, setForm] = useState({
    email: '',
    password: ''
  });

  const { setUser } = useContext(AuthContext);
  const navigate = useNavigate();
  const [errorMessage, setErrorMessage] = useState('');

  const updateForm = (prop) => (e) => {
    setForm({
      ...form,
      [prop]: e.target.value
    });
  };

  const handleKeyDown = (e) => {
    if (e.key === 'Enter') {
      onLogin();
    }
  };

  const onLogin = () => {
    if (!form.email) {
      return alert('Email is required');
    }

    if (!form.password || form.password.length < 6) {
      return alert('Password is required and must be at least 6 characters');
    }

    loginUser(form.email, form.password)
      .then((credential) => {
        setUser({
          user: credential.user
        });
      })
      .then(() => {
        navigate('/home');
      })
      .catch((error) => {
        setErrorMessage('Incorrect email or password');
        console.error('Error during login:', error);
      });
  };

  return (
    <div className="mx-auto flex min-h-screen w-full pt-28 justify-center bg-zinc-200 text-zinc-900 dark:bg-zinc-900 dark:text-zinc-200">
      <section className="flex w-[30rem] flex-col space-y-10">
        <h1 className="mb-8 text-2xl text-center font-semibold tracking-wider text-zinc-600 capitalize dark:text-zinc-100">
          Log Into Your Account
        </h1>
        <div className="w-full transform border-b-2 bg-transparent border-zinc-400 dark:border-zinc-300 text-lg duration-300 focus-within:border-teal-500 dark:focus-within:border-teal-400">
          <input
            type="email"
            name="email"
            id="email"
            value={form.email}
            onChange={updateForm('email')}
            onKeyDown={handleKeyDown}
            placeholder="Email"
            className="w-full border-none bg-transparent outline-none placeholder:italic focus:outline-none placeholder:text-zinc-500 dark:placeholder:text-zinc-400"
          />
        </div>
        <div className="w-full transform border-b-2 bg-transparent border-zinc-400 dark:border-zinc-300 text-lg duration-300 focus-within:border-teal-500 dark:focus-within:border-teal-400">
          <input
            type="password"
            name="password"
            id="password"
            value={form.password}
            onChange={updateForm('password')}
            onKeyDown={handleKeyDown}
            placeholder="Password"
            className="w-full border-none bg-transparent outline-none placeholder:italic focus:outline-none placeholder:text-zinc-500 dark:placeholder:text-zinc-400"
          />
        </div>
        <div className="flex items-center justify-between">
          <div className="flex items-start">
            <div className="flex items-center h-5">
              <input
                id="remember"
                aria-describedby="remember"
                type="checkbox"
                className="w-4 h-4 border border-gray-300 rounded bg-gray-50 focus:ring-3 focus:ring-primary-300 dark:bg-gray-700 dark:border-gray-600 dark:focus:ring-primary-600 dark:ring-offset-gray-800"
                required=""
              />
            </div>
            <div className="ml-3 text-sm">
              <label htmlFor="remember" className="text-gray-500 dark:text-gray-300">
                Remember me
              </label>
            </div>
          </div>
        </div>
        {errorMessage && <p className="error-message">{errorMessage}</p>}
        <button
          type="submit"
          onClick={onLogin}
          className="w-full text-cyan-500 bg-zinc-200 hover:bg-cyan-500 border-2 border-cyan-500 hover:text-white dark:bg-zinc-900 dark:hover:bg-cyan-400 dark:border-cyan-400 dark:text-cyan-400 dark:hover:text-white hover:font-bold focus:outline-none font-medium rounded-lg text-xl px-5 py-2.5 text-center">
          Log in
        </button>
        <p className="text-center text-lg">
          No account?{' '}
          <Link
            to="/register"
            href="#"
            className="font-medium text-cyan-500 underline-offset-4 hover:underline">
            Create One
          </Link>
        </p>
      </section>
    </div>
  );
}
