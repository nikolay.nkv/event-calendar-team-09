import dayjs from 'dayjs';
import { useContext } from 'react';
import CalendarContext from '../../../context/CalendarContext';
import CreateEventButton from '../../events/CreateEventButton';
import { useAuth } from '../../../services/firebase';

export default function CalendarHeader() {
  const currentUser = useAuth();
  const {
    dayIndex,
    setDayIndex,
    weekIndex,
    setWeekIndex,
    monthIndex,
    setMonthIndex,
    calendarView,
    setCalendarView
  } = useContext(CalendarContext);

  function handleWeekRenderMonth() {
    const calculatedDate = dayjs().day(weekIndex * 7);
    const start = calculatedDate.startOf('week').month();
    const end = calculatedDate.endOf('week').month();

    if (start === end) {
      return dayjs(new Date(dayjs().year(), monthIndex)).format('MMMM YYYY');
    } else if (start !== end) {
      return `${dayjs(new Date(dayjs().year(), monthIndex - 1)).format('MMM')}-${dayjs(
        new Date(dayjs().year(), monthIndex)
      ).format('MMM YYYY')}`;
    }
  }

  function handlePrev() {
    if (calendarView === 'month') {
      setMonthIndex(monthIndex - 1);
    } else if (calendarView === 'week') {
      setWeekIndex(weekIndex - 1);
    } else if (calendarView === 'day') {
      const lastDayOfPrevMonth = dayjs().subtract(1, 'month').endOf('month').date();
      if (dayIndex <= 1) {
        setDayIndex(lastDayOfPrevMonth);
        setMonthIndex(monthIndex - 1);
      } else if (dayIndex > 1) {
        setDayIndex(dayIndex - 1);
      }
    } else if (calendarView === 'workweek') {
      setWeekIndex(weekIndex - 1);
    }
  }

  function handleNext() {
    if (calendarView === 'month') {
      setMonthIndex(monthIndex + 1);
    } else if (calendarView === 'week') {
      if (monthIndex > 11) {
        setMonthIndex(0);
      }
      setWeekIndex(weekIndex + 1);
    } else if (calendarView === 'day') {
      const lastDayOfMonth = dayjs().endOf('month').date();
      if (dayIndex >= lastDayOfMonth) {
        setDayIndex(1);
        setMonthIndex(monthIndex + 1);
      } else if (dayIndex <= lastDayOfMonth) {
        setDayIndex(dayIndex + 1);
      }
    } else if (calendarView === 'workweek') {
      setWeekIndex(weekIndex + 1);
    }
  }

  function handleReset() {
    if (calendarView === 'month') {
      setMonthIndex(monthIndex === dayjs().month() ? monthIndex + Math.random() : dayjs().month());
    } else if (calendarView === 'week' || calendarView === 'workweek') {
      setWeekIndex(0);
    } else if (calendarView === 'day') {
      setDayIndex(dayjs().date());
      setMonthIndex(monthIndex === dayjs().month() ? monthIndex + Math.random() : dayjs().month());
    }
  }

  function handleCalendarView(e) {
    setCalendarView(e.target.value);
  }

  return (
    <header className="px-4 flex items-center relative">
      <h1 className="mr-10 text-2xl text-zinc-500 dark:text-zinc-200 font-bold">Calendar</h1>
      <button
        onClick={handleReset}
        className="bg-zinc-200 hover:bg-zinc-50 dark:bg-zinc-800 text-zinc-500 hover:text-zinc-500 shadow-sm shadow-zinc-400 dark:text-zinc-300 dark:shadow-black rounded-full dark:hover:bg-zinc-600 dark:hover:text-zinc-100 px-5 py-2.5 mr-5 text-base font-bold ml-2">
        Today
      </button>
      <button
        onClick={() => {
          handlePrev();
          handleWeekRenderMonth();
        }}>
        <span className="material-icons-outlined cursor-pointer text-zinc-600 dark:text-zinc-200 mx-2 mr-7">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="currentColor"
            className="w-6 h-6">
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M10.5 19.5L3 12m0 0l7.5-7.5M3 12h18"
            />
          </svg>
        </span>
      </button>
      <button
        onClick={() => {
          handleNext();
          handleWeekRenderMonth();
        }}>
        <span className="material-icons-outlined cursor-pointer text-zinc-600 dark:text-zinc-200 mx-2">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="currentColor"
            className="w-6 h-6">
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M13.5 4.5L21 12m0 0l-7.5 7.5M21 12H3"
            />
          </svg>
        </span>
      </button>
      <h2 className="ml-5 text-xl text-zinc-500 dark:text-zinc-200 mr-5 font-semibold">
        {calendarView === 'day'
          ? dayjs(new Date(dayjs().year(), monthIndex)).format('MMMM YYYY')
          : null}
        {calendarView === 'week'
          ? dayjs(new Date(dayjs().year(), monthIndex)).format('MMMM YYYY')
          : null}
        {calendarView === 'month'
          ? dayjs(new Date(dayjs().year(), monthIndex)).format('MMMM YYYY')
          : null}
        {calendarView === 'workweek'
          ? dayjs(new Date(dayjs().year(), monthIndex)).format('MMMM YYYY')
          : null}
      </h2>
      <div className="relative w-full lg:max-w-sm">
        <select
          defaultValue={'month'}
          onChange={handleCalendarView}
          className=" text-zinc-600 dark:text-zinc-300 h-11 pl-5 pr-10 bg-zinc-200 dark:bg-zinc-800 dark:hover:bg-zinc-600 dark:shadow-black rounded-full focus:outline-none appearance-none text-lg shadow-sm dark:hover:text-zinc-50 shadow-zinc-400 hover:bg-zinc-50 active:bg-zinc-50 active:shadow-sm">
          <option value="day">Day</option>
          <option value="week">Week</option>
          <option value="month">Month</option>
          <option value="workweek">Work Week</option>
        </select>
      </div>
      <div className="ml-auto">
        {currentUser !== null && (
          <CreateEventButton className="text-gray-500 transition-all duration-300 transform group-hover:translate-x-full ease text-lg font-semibold" />
        )}
      </div>
    </header>
  );
}
