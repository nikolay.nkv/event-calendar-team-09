import React from 'react';
import dayjs from 'dayjs';
import { getMonth } from '../../../services/calendarService';
import { useState, useEffect, useContext } from 'react';
import CalendarHeader from './CalendarHeader';
import Sidebar from './Sidebar';
import Month from './Month';
import CalendarContext from '../../../context/CalendarContext';
import Week from '../WeekCalendar/Week';
import Day from '../DayCalendar/Day';
import WorkWeek from '../WeekCalendar/WorkWeek';

export default function Calendar() {
  const [currentDay] = useState(dayjs().date());
  const [currenMonth, setCurrentMonth] = useState(getMonth());
  const { monthIndex, calendarView } = useContext(CalendarContext);

  useEffect(() => {
    setCurrentMonth(getMonth(monthIndex));
  }, [monthIndex]);

  return (
    <React.Fragment>
      <div className="bg-zinc-200 dark:bg-zinc-900 pt-4 pb-10">
        <div className="bg-zinc-100 dark:bg-zinc-800 shadow-lg shadow-zinc-500 dark:shadow-inner dark:shadow-black ml-6 mr-6 pt-1 pb-6 rounded-3xl h-full">
          <div className="h-screen flex flex-col">
            <CalendarHeader />
            <div className="flex flex-1 overflow-x-hidden h-full">
              <Sidebar />
              {calendarView === 'day' ? <Day day={currentDay} /> : null}
              {calendarView === 'week' ? <Week month={currenMonth} /> : null}
              {calendarView === 'month' ? <Month month={currenMonth} /> : null}
              {calendarView === 'workweek' ? <WorkWeek month={currenMonth} /> : null}
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
