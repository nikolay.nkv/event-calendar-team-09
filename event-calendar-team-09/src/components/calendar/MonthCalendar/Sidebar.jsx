import SmallCalendar from './SmallCalendar';
import Labels from './Labels';

export default function Sidebar() {
  return (
    <aside className="border-t border-b dark:border-zinc-900 p-5 w-64">
      <SmallCalendar />
      <Labels />
    </aside>
  );
}
