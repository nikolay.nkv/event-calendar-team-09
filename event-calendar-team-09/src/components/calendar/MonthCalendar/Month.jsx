import React, { useState } from 'react';
import MonthDay from './MonthDay';
import EventDetails from '../../events/EventDetails';
import PropTypes from 'prop-types';

export default function Month({ month }) {
  const [selectedEvent, setSelectedEvent] = useState(null);

  const openEventDetails = (event) => {
    setSelectedEvent(event);
  };

  Month.propTypes = {
    month: PropTypes.array.isRequired
  };

  return (
    <div className="flex-1 grid grid-cols-7 grid-rows-5">
      {month.map((row, i) => (
        <React.Fragment key={i}>
          {row.map((day, idx) => (
            <MonthDay day={day} key={idx} rowIdx={i} openEventDetails={openEventDetails} />
          ))}
          {selectedEvent && <EventDetails event={selectedEvent} />}
        </React.Fragment>
      ))}
    </div>
  );
}
