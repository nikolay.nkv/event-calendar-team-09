import dayjs from 'dayjs';
import { useContext, useState, useEffect } from 'react';
import CalendarContext from '../../../context/CalendarContext';
import { getAllEvents } from '../../../services/eventService';
import { Link } from 'react-router-dom';
import { useAuth } from '../../../services/firebase';
import PropTypes from 'prop-types';

export default function MonthDay({ day, rowIdx, openEventDetails }) {
  const currentUser = useAuth();
  const [dayEvents, setDayEvents] = useState([]);
  const { selectedCategories, setDaySelected, filteredEvents, setSelectedEvent } =
    useContext(CalendarContext);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const events = await getAllEvents();
        if (selectedCategories.length === 0) {
          const filteredEvents = events.filter(
            (evt) => dayjs(evt.startDate).format('DD-MM-YY') === day.format('DD-MM-YY')
          );
          const filteredPublicEvents =
            currentUser === null ? filteredEvents.filter((evt) => evt.isPublic) : filteredEvents;
          setDayEvents(filteredPublicEvents);
        } else if (selectedCategories.length !== 0) {
          const filteredEvents = events
            .filter((event) => {
              return selectedCategories.length === 0 || selectedCategories.includes(event.category);
            })
            .filter((evt) => dayjs(evt.startDate).format('DD-MM-YY') === day.format('DD-MM-YY'));
          const filteredPublicEvents =
            currentUser === null ? filteredEvents.filter((evt) => evt.isPublic) : filteredEvents;
          setDayEvents(filteredPublicEvents);
        }
      } catch (error) {
        console.error('Error fetching events:', error);
      }
    };

    fetchData();
  }, [selectedCategories, filteredEvents, day, currentUser]);

  function getCurrentDayClass() {
    return day.format('DD-MM-YY') === dayjs().format('DD-MM-YY')
      ? 'bg-cyan-500 text-white rounded-full w-7'
      : '';
  }

  function getColorClass(category) {
    const categoryColors = {
      party: 'bg-violet-400',
      education: 'bg-emerald-400',
      work: 'bg-sky-400',
      birthday: 'bg-pink-300',
      holiday: 'bg-pink-500'
    };

    return categoryColors[category] || 'bg-blue-200';
  }

  MonthDay.propTypes = {
    day: PropTypes.object.isRequired,
    rowIdx: PropTypes.number.isRequired,
    openEventDetails: PropTypes.func.isRequired
  };

  return (
    <div className="border-b border-t border-l border-zinc-200 dark:border-zinc-900 flex flex-col w-[10rem]">
      <header className="flex flex-col items-center">
        {rowIdx === 0 && (
          <p className="text-sm text-zinc-700 dark:text-zinc-200 mt-1">
            {day.format('ddd').toUpperCase()}
          </p>
        )}
        <p className={`text-sm dark:text-zinc-100 p-1 my-1 text-center  ${getCurrentDayClass()}`}>
          {day.format('DD')}
        </p>
      </header>
      <div
        className="flex-1 cursor-pointer overflow-y-auto"
        onClick={() => {
          setDaySelected(day);
        }}>
        {dayEvents.map((evt, idx) => (
          <Link to={`/event-details/${evt.id}`} key={idx}>
            <div
              className={`p-1 ${getColorClass(
                evt.category
              )} text-white dark:text-black text-xs rounded-lg shadow-md shadow-zinc-500 dark:shadow-black cursor-pointer hover:bg-cyan-300 hover:text-black hover:shadow-inner ml-1 mr-2`}
              onClick={() => {
                openEventDetails(evt);
                setSelectedEvent(evt);
              }}>
              {evt.title.length > 22 ? `${evt.title.slice(0, 22)}...` : evt.title}
            </div>
          </Link>
        ))}
      </div>
    </div>
  );
}
