import React, { useContext } from 'react';
import CalendarContext from '../../../context/CalendarContext';

export default function Labels() {
  const { selectedCategories, setSelectedCategories } = useContext(CalendarContext);
  const categories = ['work', 'education', 'party', 'birthday', 'holiday'];

  const handleCategoryChange = (category) => {
    if (selectedCategories.includes(category)) {
      setSelectedCategories((prevCategories) =>
        prevCategories.filter((prevCategory) => prevCategory !== category)
      );
    } else {
      setSelectedCategories((prevCategories) => [...prevCategories, category]);
    }
  };

  function getColorClass(category) {
    const categoryColors = {
      party: 'bg-violet-400',
      education: 'bg-emerald-400',
      work: 'bg-sky-400',
      birthday: 'bg-pink-300',
      holiday: 'bg-pink-500'
    };

    return categoryColors[category] || 'bg-blue-200';
  }

  return (
    <React.Fragment>
      <div className="ml-2">
        <p className="text-zinc-600 dark:text-zinc-300 font-bold text-base mt-10 mb-2">Labels</p>
        {categories.map((category) => (
          <div key={category} className="flex items-center mb-2">
            <input
              type="checkbox"
              id={category}
              checked={selectedCategories.includes(category)}
              onChange={() => handleCategoryChange(category)}
              className={`h-4 w-4 accent-cyan-500 mr-2 ${getColorClass(category)}`}
            />
            <label
              htmlFor={category}
              className={`font-semibold text-white dark:text-black p-0.5 rounded ${getColorClass(
                category
              )}`}>
              {category}
            </label>
          </div>
        ))}
      </div>
    </React.Fragment>
  );
}
