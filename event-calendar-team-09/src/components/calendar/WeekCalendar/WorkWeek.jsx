import { useState, useContext, useEffect } from 'react';
import dayjs from 'dayjs';
import CalendarContext from '../../../context/CalendarContext';
import WeekDay from './WeekDay';

export default function WorkWeek() {
  const { weekIndex, setMonthIndex, setSelectedEvent } = useContext(CalendarContext);
  const [currentWeekDays, setCurrentWeekDays] = useState([]);

  useEffect(() => {
    const todayDate = dayjs();
    const startOfWeek = todayDate.startOf('isoWeek').add(weekIndex * 7, 'day');

    setMonthIndex(startOfWeek.month());
    const weekDays = Array.from({ length: 7 }, (_, idx) => startOfWeek.add(idx, 'day'));
    setCurrentWeekDays(weekDays);
  }, [weekIndex, setMonthIndex]);

  const openEventDetails = (event) => {
    setSelectedEvent(event);
  };

  return (
    <div className="flex-1 grid grid-cols-5 ">
      {currentWeekDays.map((day, idx) => {
        if (day.day() >= 1 && day.day() <= 5) {
          return (
            <WeekDay
              day={day}
              key={idx}
              isFirstDayOfWeek={idx === 0}
              openEventDetails={openEventDetails}
            />
          );
        } else {
          return null;
        }
      })}
    </div>
  );
}
