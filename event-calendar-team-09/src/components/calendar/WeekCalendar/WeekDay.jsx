import dayjs from 'dayjs';
import { useContext, useState, useEffect } from 'react';
import CalendarContext from '../../../context/CalendarContext';
import { Link } from 'react-router-dom';
import { getAllEvents } from '../../../services/eventService';
import { useAuth } from '../../../services/firebase';
import PropTypes from 'prop-types';

export default function WeekDay({ day, isFirstDayOfWeek, openEventDetails }) {
  const currentUser = useAuth();
  const [dayEvents, setDayEvents] = useState([]);
  const { selectedCategories, setDaySelected, filteredEvents, setSelectedEvent } =
    useContext(CalendarContext);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const events = await getAllEvents();
        const filteredEvents = events.filter(
          (evt) => dayjs(evt.startDate).format('DD-MM-YY') === day.format('DD-MM-YY')
        );
        setDayEvents(filteredEvents);
      } catch (error) {
        console.error('Error fetching events:', error);
      }
    };

    fetchData();
  }, [filteredEvents, day]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const events = await getAllEvents();
        if (selectedCategories.length === 0) {
          const filteredEvents = events.filter(
            (evt) => dayjs(evt.startDate).format('DD-MM-YY') === day.format('DD-MM-YY')
          );

          const filteredPublicEvents =
            currentUser === null ? filteredEvents.filter((evt) => evt.isPublic) : filteredEvents;
          setDayEvents(filteredPublicEvents);
        } else if (selectedCategories.length !== 0) {
          const filteredEvents = events
            .filter((event) => {
              return selectedCategories.length === 0 || selectedCategories.includes(event.category);
            })
            .filter((evt) => dayjs(evt.startDate).format('DD-MM-YY') === day.format('DD-MM-YY'));

          const filteredPublicEvents =
            currentUser === null ? filteredEvents.filter((evt) => evt.isPublic) : filteredEvents;
          setDayEvents(filteredPublicEvents);
        }
      } catch (error) {
        console.error('Error fetching events:', error);
      }
    };

    fetchData();
  }, [selectedCategories, filteredEvents, day, currentUser]);

  function getCurrentDayClass() {
    return day.format('DD-MM-YY') === dayjs().format('DD-MM-YY')
      ? 'bg-cyan-500 text-white rounded-full w-7'
      : '';
  }

  function getColorClass(category) {
    const categoryColors = {
      party: 'bg-violet-400',
      education: 'bg-emerald-400',
      work: 'bg-sky-400',
      birthday: 'bg-pink-300',
      holiday: 'bg-pink-500'
    };

    return categoryColors[category] || 'bg-blue-200';
  }

  WeekDay.propTypes = {
    day: PropTypes.object.isRequired,
    isFirstDayOfWeek: PropTypes.bool.isRequired,
    openEventDetails: PropTypes.func.isRequired
  };

  return (
    <div className=" dark:text-zinc-50 text-zinc-700 border border-zinc-200 dark:border-zinc-900 flex flex-col w-full">
      <div
        className="flex-1 cursor-pointer overflow-y-auto"
        onClick={() => {
          setDaySelected(day);
        }}>
        <header className="flex flex-col items-center">
          <p className="text-sm mt-1">{day.format('ddd').toUpperCase()}</p>
          <p className={`text-sm p-1 my-1 text-center ${getCurrentDayClass()}`}>
            {day.format('DD')}
          </p>
        </header>
        <div className="flex flex-col">
          {Array.from({ length: 24 }, (_, hour) => (
            <div
              key={hour}
              className="text-left dark:text-zinc-300 text-zinc-500 py-2 text-sm cursor-pointer border-b border-zinc-200 dark:border-zinc-900 pl-2 h-20"
              onClick={() => {
                setDaySelected(day);
              }}>
              {isFirstDayOfWeek && dayjs().startOf('day').add(hour, 'hour').format('HH:mm')}
              {dayEvents.map((evt) => {
                const eventHour = dayjs(evt.startDate).hour();
                if (eventHour === hour) {
                  return (
                    <Link
                      to={`/event-details/${evt.id}`}
                      key={evt.id}
                      className={`flex flex-col p-1 ${getColorClass(
                        evt.category
                      )} text-white dark:text-black text-xs rounded-lg shadow-md shadow-zinc-500 dark:shadow-black cursor-pointer hover:bg-cyan-300 hover:text-black hover:shadow-inner ml-1 mr-2 overflow-x-auto`}
                      onClick={() => {
                        openEventDetails(evt);
                        setSelectedEvent(evt);
                      }}>
                      {evt.title.length > 20 ? `${evt.title.slice(0, 20)}...` : evt.title}
                    </Link>
                  );
                }
                return null;
              })}
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
