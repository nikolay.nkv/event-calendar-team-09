import { useState, useContext, useEffect } from 'react';
import dayjs from 'dayjs';
import CalendarContext from '../../../context/CalendarContext';

import WeekDay from './WeekDay';

export default function Week() {
  const { weekIndex, setMonthIndex, setSelectedEvent } = useContext(CalendarContext);
  const [currentWeekDays, setCurrentWeekDays] = useState();

  useEffect(() => {
    const todayDate = dayjs();
    const startOfWeek = todayDate.day(weekIndex * 7).startOf('day');
    setMonthIndex(
      todayDate
        .day(weekIndex * 7)
        .endOf('week')
        .month()
    );

    setCurrentWeekDays(Array.from({ length: 7 }, (_, idx) => startOfWeek.add(idx, 'day')));
  }, [weekIndex, setMonthIndex]);

  const openEventDetails = (event) => {
    setSelectedEvent(event);
  };

  return (
    <div className="flex-1 grid grid-cols-7">
      {currentWeekDays &&
        currentWeekDays.map((day, idx) => (
          <WeekDay
            day={day}
            key={idx}
            isFirstDayOfWeek={idx === 0}
            openEventDetails={openEventDetails}
          />
        ))}
    </div>
  );
}
