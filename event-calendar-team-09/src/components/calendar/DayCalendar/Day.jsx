import { useState, useContext, useEffect } from 'react';
import dayjs from 'dayjs';
import CalendarContext from '../../../context/CalendarContext';
import DayHour from './DayHour';

export default function Day() {
  const [currentDay, setCurrentDay] = useState(dayjs());
  const { dayIndex, setSelectedEvent } = useContext(CalendarContext);

  useEffect(() => {
    setCurrentDay(dayjs().date(dayIndex));
  }, [dayIndex]);

  const openEventDetails = (event) => {
    setSelectedEvent(event);
  };

  return (
    <div className="flex-1 grid grid-cols-1 h-full">
      <DayHour day={currentDay} openEventDetails={openEventDetails} />
    </div>
  );
}
