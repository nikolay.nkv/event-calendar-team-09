import dayjs from 'dayjs';
import { useContext, useState, useEffect } from 'react';
import CalendarContext from '../../../context/CalendarContext';
import { getAllEvents } from '../../../services/eventService';
import { Link } from 'react-router-dom';
import { useAuth } from '../../../services/firebase';
import PropTypes from 'prop-types';

export default function DayHour({ day, openEventDetails }) {
  const currentUser = useAuth();
  const [dayEvents, setDayEvents] = useState([]);
  const {
    dayIndex,
    monthIndex,
    setDaySelected,
    filteredEvents,
    setSelectedEvent,
    selectedCategories
  } = useContext(CalendarContext);

  const date = new Date(2023, monthIndex, dayIndex);
  const dayJsDate = dayjs(date);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const events = await getAllEvents();
        if (selectedCategories.length === 0) {
          const filteredEvents = events.filter(
            (evt) => dayjs(evt.startDate).format('DD-MM-YY') === day.format('DD-MM-YY')
          );

          const filteredPublicEvents =
            currentUser === null ? filteredEvents.filter((evt) => evt.isPublic) : filteredEvents;
          setDayEvents(filteredPublicEvents);
        } else if (selectedCategories.length !== 0) {
          const filteredEvents = events
            .filter((event) => {
              return selectedCategories.length === 0 || selectedCategories.includes(event.category);
            })
            .filter((evt) => dayjs(evt.startDate).format('DD-MM-YY') === day.format('DD-MM-YY'));

          const filteredPublicEvents =
            currentUser === null ? filteredEvents.filter((evt) => evt.isPublic) : filteredEvents;
          setDayEvents(filteredPublicEvents);
        }
      } catch (error) {
        console.error('Error fetching events:', error);
      }
    };

    fetchData();
  }, [selectedCategories, filteredEvents, day, currentUser]);

  function getCurrentDayClass() {
    return dayJsDate.format('DD-MM-YY') === dayjs().format('DD-MM-YY')
      ? 'bg-cyan-500 text-white rounded-full w-7'
      : '';
  }

  function getColorClass(category) {
    const categoryColors = {
      party: 'bg-violet-400',
      education: 'bg-emerald-400',
      work: 'bg-sky-400',
      birthday: 'bg-pink-300',
      holiday: 'bg-pink-500'
    };

    return categoryColors[category] || 'bg-blue-200';
  }

  DayHour.propTypes = {
    day: PropTypes.object.isRequired,
    openEventDetails: PropTypes.func.isRequired
  };

  return (
    <div className="border border-zinc-200 dark:border-zinc-900 flex flex-col w-full text-zinc-800 dark:text-zinc-100">
      <div
        className="flex-1 cursor-pointer"
        onClick={() => {
          setDaySelected(day);
        }}>
        <header className="flex flex-col items-center">
          <p className="text-sm mt-1">{dayJsDate.format('ddd').toUpperCase()}</p>
          <p className={`text-sm p-1 my-1 text-center  ${getCurrentDayClass()}`}>
            {dayJsDate.format('DD')}
          </p>
          <p className="text-sm mt-1">{dayJsDate.format('MMMM')}</p>
        </header>
        <div className="border-r border-zinc-200 dark:border-zinc-900 flex flex-col">
          {Array.from({ length: 24 }, (_, hour) => (
            <div
              key={hour}
              className="text-left py-8 text-sm text-zinc-600 dark:text-zinc-200 cursor-pointer border-b border-zinc-200 dark:border-zinc-900 pl-1">
              {dayjs().startOf('day').add(hour, 'hour').format('HH:mm')}
              {dayEvents.map((evt) => {
                const eventHour = dayjs(evt.startDate).hour();
                if (eventHour === hour) {
                  return (
                    <Link
                      to={`/event-details/${evt.id}`}
                      key={evt.id}
                      style={{ width: `${evt.title.length * 8}px` }}
                      className={`p-1  ${getColorClass(
                        evt.category
                      )} text-white dark:text-black text-2xl p-4 rounded-lg shadow-md shadow-zinc-500 dark:shadow-black cursor-pointer hover:bg-cyan-300 hover:text-black hover:shadow-inner ml-4 mr-2`}
                      onClick={() => {
                        setSelectedEvent(evt);
                        openEventDetails(evt);
                      }}>
                      {evt.title}
                    </Link>
                  );
                }
                return null;
              })}
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
