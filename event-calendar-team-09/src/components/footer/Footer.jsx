export function Footer() {
  return (
    <footer className="flex flex-col items-center bg-zinc-100 dark:bg-zinc-900 shadow-inner shadow-zinc-400 dark:shadow-black text-center text-zinc-700 dark:text-zinc-100">
      <div className="container px-6 pt-4">
        <div className="mb-4 flex justify-center">
          <a
            href="https://gitlab.com/nikolay.nkv/event-calendar-team-09"
            type="button"
            className="m-1 h-9 w-9 rounded-full hover:shadow-md dark:hover:shadow-inner hover:shadow-zinc-800 dark:hover:shadow-black bg-zinc-700 dark:bg-zinc-200 uppercase leading-normal transition duration-150 ease-in-out hover:bg-zinc-200 dark:hover:bg-zinc-700 focus:outline-none focus:ring-0"
            data-te-ripple-init
            data-te-ripple-color="light">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              fill="none"
              stroke="currentColor"
              strokeWidth="2"
              strokeLinecap="round"
              strokeLinejoin="round"
              className="mx-auto h-full w-5 text-zinc-50 hover:text-zinc-900 dark:text-zinc-900 dark:hover:text-zinc-50">
              <path d="M22.65 14.39L12 22.13 1.35 14.39a.84.84 0 0 1-.3-.94l1.22-3.78 2.44-7.51A.42.42 0 0 1 4.82 2a.43.43 0 0 1 .58 0 .42.42 0 0 1 .11.18l2.44 7.49h8.1l2.44-7.51A.42.42 0 0 1 18.6 2a.43.43 0 0 1 .58 0 .42.42 0 0 1 .11.18l2.44 7.51L23 13.45a.84.84 0 0 1-.35.94z"></path>
            </svg>
          </a>
        </div>
      </div>
      <div className="w-full pb-4 text-center">
        © 2023 Copyright:
        <a className="text-whitehite"> SpectraSchedule</a>
      </div>
    </footer>
  );
}

export default Footer;
