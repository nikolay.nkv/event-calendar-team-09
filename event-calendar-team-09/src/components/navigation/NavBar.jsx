import { Link, useNavigate } from 'react-router-dom';
import { useContext, useState, useEffect } from 'react';
import { AuthContext } from '../../context/AuthContext';
import logo from '../../assets/logo.png';
import logoWhite from '../../assets/logoWhite.png';
import { logoutUser } from '../../services/authService';
import { storage } from '../../services/firebase';
import { getDownloadURL, ref } from 'firebase/storage';
import { useAuth } from '../../services/firebase';
import { getUserByHandle, getUserData } from '../../services/userService';
import CalendarContext from '../../context/CalendarContext';

export function NavBar() {
  const currentUser = useAuth();
  const { setShowEventModal } = useContext(CalendarContext);
  const { user, setUser } = useContext(AuthContext);
  const [, setUserProfile] = useState(null);
  const [showLogoutModal, setShowLogoutModal] = useState(false);
  const [profilePhotoURL, setProfilePhotoURL] = useState('');
  const [darkMode, setDarkMode] = useState(false);

  async function fetchUserProfile(handle) {
    try {
      const userProfileSnapshot = await getUserByHandle(handle);
      return userProfileSnapshot.val();
    } catch (error) {
      console.error('Error fetching user profile:', error);
      return null;
    }
  }

  useEffect(() => {
    async function fetchUserProfileData() {
      if (currentUser) {
        const fetchedUserProfile = await fetchUserProfile(currentUser.handle);
        setUserProfile(fetchedUserProfile);
      }
    }

    fetchUserProfileData();
  }, [currentUser]);

  useEffect(() => {
    if (currentUser) {
      getUserData(currentUser.uid)
        .then((snapshot) => {
          if (snapshot.exists()) {
            const userData = snapshot.val();
            const firstUser = Object.values(userData)[0];
            setUserProfile(firstUser);
          }
        })
        .catch((error) => {
          console.error('Error fetching user data:', error);
        });
    }
  }, [currentUser]);

  const handleShowLogoutModal = () => setShowLogoutModal(true);
  const handleCloseLogoutModal = () => setShowLogoutModal(false);
  const navigate = useNavigate();
  const onLogout = () => {
    logoutUser().then(() => {
      setUser({
        user: null
      });
      handleCloseLogoutModal();
      navigate('/home');
    });
  };

  useEffect(() => {
    if (currentUser) {
      const userPhotoRef = ref(storage, `${currentUser.uid}.png`);
      getDownloadURL(userPhotoRef)
        .then((url) => {
          setProfilePhotoURL(url);
        })
        .catch((error) => {
          console.error('Error fetching profile photo:', error);
        });
    } else {
      setProfilePhotoURL(
        'https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png'
      );
    }
  }, [currentUser]);

  const toggleDarkMode = () => {
    const newDarkMode = !darkMode;
    setDarkMode(newDarkMode);
    document.documentElement.classList.toggle('dark', newDarkMode);
    localStorage.setItem('darkMode', newDarkMode ? 'true' : 'false');
  };

  useEffect(() => {
    const storedDarkMode = localStorage.getItem('darkMode');
    if (storedDarkMode === 'true') {
      setDarkMode(true);
      document.documentElement.classList.add('dark');
    } else {
      setDarkMode(false);
      document.documentElement.classList.remove('dark');
    }
  }, []);

  return (
    <>
      {user !== null && showLogoutModal && (
        <div className="fixed inset-0 flex items-center justify-center z-50">
          <div className="absolute inset-0 bg-zinc-800 dark:bg-black opacity-75"></div>
          <div className="fixed inset-0 flex items-center justify-center">
            <div className="bg-zinc-100 dark:bg-zinc-900 dark:text-zinc-100 text-zinc-600 p-10 rounded-2xl shadow-lg font-mono">
              <div className=" flex justify-center text-zinc-500 dark:text-zinc-400 mb-2">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                  className="w-10 h-10">
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15m3 0l3-3m0 0l-3-3m3 3H9"
                  />
                </svg>
              </div>

              <h2 className="text-xl font-semibold mb-10 mt-5 ml-5 mr-5 text-center text-zinc-500 dark:text-zinc-400">
                Are you sure you want to log out?
              </h2>
              <div className="flex justify-center mb-5">
                <button
                  onClick={onLogout}
                  href="#_"
                  className="relative inline-flex items-center justify-center px-4 py-2
                  overflow-hidden font-large text-indigo-600 transition duration-300 ease-out
                  border-2 border-rose-500 dark:border-rose-500 bg-zinc-100 dark:bg-zinc-900 rounded-full shadow-md group mr-6">
                  <span className="absolute inset-0 flex items-center justify-center w-full h-full text-white duration-300 -translate-x-full bg-rose-500 group-hover:translate-x-0 ease">
                    <svg
                      className="w-6 h-6"
                      fill="none"
                      stroke="currentColor"
                      viewBox="0 0 24 24"
                      xmlns="http://www.w3.org/2000/svg">
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        d="M14 5l7 7m0 0l-7 7m7-7H3"></path>
                    </svg>
                  </span>
                  <span className="absolute flex items-center justify-center w-full h-full text-rose-500 transition-all duration-300 transform group-hover:translate-x-full ease text-lg font-semibold">
                    Log out
                  </span>
                  <span className="relative invisible">Button Text</span>
                </button>
                <button
                  onClick={handleCloseLogoutModal}
                  href="#_"
                  className="inline-flex items-center justify-center w-full px-6 py-3 font-bold leading-6 text-zinc-500 dark:text-zinc-300 bg-zinc-50 dark:bg-zinc-900 border-2 border-zinc-400 dark:border-zinc-300 rounded-full md:w-auto hover:bg-zinc-400 dark:hover:bg-zinc-300 hover:text-white dark:hover:text-zinc-900 text-lg focus:outline-none shadow-md">
                  Cancel
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
      <nav className="bg-zinc-200 text-zinc-600 dark:bg-zinc-900 dark:text-zinc-400 shadow h-20 flex items-center justify-between w-full">
        <div className="flex items-center ml-2">
          <Link to="/home" className="h-12 w-20 mx-auto mb-4">
            <img
              className="h-16 w-16 mx-auto"
              src={darkMode ? logoWhite : logo}
              alt="SpectraSchedule logo"
            />
          </Link>
          <span className="text-lg font-semibold">SpectraSchedule</span>
        </div>
        {user === null && (
          <div className="flex">
            <Link
              to="/log-in"
              href="#_"
              className="relative inline-flex items-center justify-center p-3 px-5 py-2 overflow-hidden font-medium transition duration-300 ease-out border-2 border-cyan-500  rounded-full shadow-md group">
              <span className="absolute inset-0 flex items-center justify-center w-full h-full text-white duration-300 -translate-x-full bg-cyan-500  group-hover:translate-x-0 ease">
                <svg
                  className="w-6 h-6"
                  fill="none"
                  stroke="currentColor"
                  viewBox="0 0 24 24"
                  xmlns="http://www.w3.org/2000/svg">
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M14 5l7 7m0 0l-7 7m7-7H3"></path>
                </svg>
              </span>
              <span className="absolute flex items-center justify-center w-full h-full text-cyan-500 transition-all duration-300 transform group-hover:translate-x-full ease font-semibold">
                Log in
              </span>
              <span className="relative invisible">Log in</span>
            </Link>
            <Link
              to="/register"
              href="#_"
              className="relative inline-flex items-center justify-center p-3 px-6 py-2 ml-5 overflow-hidden font-medium text-indigo-600 transition duration-300 ease-out border-2 border-teal-500 rounded-full shadow-md group">
              <span className="absolute inset-0 flex items-center justify-center w-full h-full text-white duration-300 -translate-x-full bg-teal-500 group-hover:translate-x-0 ease">
                <svg
                  className="w-6 h-6"
                  fill="none"
                  stroke="currentColor"
                  viewBox="0 0 24 24"
                  xmlns="http://www.w3.org/2000/svg">
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M14 5l7 7m0 0l-7 7m7-7H3"></path>
                </svg>
              </span>
              <span className="absolute flex items-center justify-center w-full h-full text-teal-500 transition-all duration-300 transform group-hover:translate-x-full ease font-semibold">
                Sign up
              </span>
              <span className="relative invisible">Sign up</span>
            </Link>
          </div>
        )}
        <div className="flex items-center ml-4">
          {user !== null && (
            <Link
              to="/search"
              type="button"
              className={`inline-flex flex-col items-center justify-center rounded-full h-14 w-14 mr-2 ml-2 transition-transform transform hover:scale-110 ${
                darkMode ? 'hover:bg-black' : 'hover:bg-zinc-300'
              } rounded-full h-14 w-14 mr-2 group`}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className={`w-7 h-7 ${
                  darkMode
                    ? 'text-zinc-300 group-hover:text-amber-300 '
                    : 'text-zinc-600 group-hover:text-amber-500'
                } `}>
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
                />
              </svg>
            </Link>
          )}
          <button
            className={`inline-flex flex-col items-center justify-center rounded-full h-14 w-14 mr-2 ml-2 transition-transform transform hover:scale-110 ${
              darkMode ? 'hover:bg-amber-100' : 'hover:bg-sky-950'
            } rounded-full h-14 w-14 mr-4 ml-2 group`}
            onClick={toggleDarkMode}>
            {darkMode ? (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className={`w-8 h-8 ${
                  darkMode
                    ? 'text-zinc-300 group-hover:text-amber-500 '
                    : 'text-zinc-600 group-hover:text-amber-500'
                } `}>
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M12 3v2.25m6.364.386l-1.591 1.591M21 12h-2.25m-.386 6.364l-1.591-1.591M12 18.75V21m-4.773-4.227l-1.591 1.591M5.25 12H3m4.227-4.773L5.636 5.636M15.75 12a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0z"
                />
              </svg>
            ) : (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className={`w-7 h-7 ${
                  darkMode
                    ? 'text-zinc-300 group-hover:text-amber-300 '
                    : 'text-zinc-600 group-hover:text-sky-200'
                } `}>
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M21.752 15.002A9.718 9.718 0 0118 15.75c-5.385 0-9.75-4.365-9.75-9.75 0-1.33.266-2.597.748-3.752A9.753 9.753 0 003 11.25C3 16.635 7.365 21 12.75 21a9.753 9.753 0 009.002-5.998z"
                />
              </svg>
            )}
          </button>
          {user !== null && (
            <Link
              to="/userprofile"
              type="button"
              className="inline-flex flex-col items-center justify-center rounded-full h-14 w-14 mr-2 ml-2 transition-transform transform hover:scale-110 shadow-sm hover:border-2 hover:border-cyan-400 hover:shadow-cyan-400">
              <img
                src={
                  profilePhotoURL ||
                  'https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png'
                }
                alt="Profile"
                className="w-12 h-12 rounded-full"
              />
            </Link>
          )}
          {user !== null && (
            <button
              className={`inline-flex flex-col items-center justify-center rounded-full h-14 w-14 mr-6 ml-2 transition-transform transform hover:scale-110 text-rose-500  ${
                darkMode
                  ? 'hover:bg-rose-700 focus:text-rose-100 hover:text-rose-300'
                  : 'hover:bg-rose-100 focus:text-rose-500'
              } focus:outline-none`}
              onClick={handleShowLogoutModal}>
              <svg
                className="h-6 w-6"
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round">
                <path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path>
                <polyline points="16 17 21 12 16 7"></polyline>
                <line x1="21" y1="12" x2="9" y2="12"></line>
              </svg>
            </button>
          )}
        </div>
      </nav>
      {user !== null && (
        <div
          className={`w-full ${
            darkMode ? 'bg-zinc-900 shadow-black' : 'bg-zinc-100 shadow-zinc-400'
          } shadow-inner`}>
          <div className="grid h-full max-w-lg grid-cols-4 mx-auto font-medium pt-3 pb-2">
            <Link
              to="/home"
              type="button"
              className={`inline-flex flex-col items-center justify-center px-12 rounded-full h-14 group ${
                darkMode ? 'hover:bg-black' : 'hover:bg-zinc-300'
              }`}>
              <svg
                className={`w-6 h-6 mb-1 ${
                  darkMode
                    ? 'text-zinc-200 group-hover:text-cyan-400'
                    : 'text-zinc-500 group-hover:text-cyan-600'
                }`}
                fill="currentColor"
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
                aria-hidden="true">
                <path d="M11.47 3.84a.75.75 0 011.06 0l8.69 8.69a.75.75 0 101.06-1.06l-8.689-8.69a2.25 2.25 0 00-3.182 0l-8.69 8.69a.75.75 0 001.061 1.06l8.69-8.69z" />
                <path d="M12 5.432l8.159 8.159c.03.03.06.058.091.086v6.198c0 1.035-.84 1.875-1.875 1.875H15a.75.75 0 01-.75-.75v-4.5a.75.75 0 00-.75-.75h-3a.75.75 0 00-.75.75V21a.75.75 0 01-.75.75H5.625a1.875 1.875 0 01-1.875-1.875v-6.198a2.29 2.29 0 00.091-.086L12 5.43z" />
              </svg>
              <span
                className={`text-sm ${
                  darkMode
                    ? 'text-zinc-200 group-hover:text-cyan-400'
                    : 'text-zinc-500 group-hover:text-cyan-600'
                }`}>
                Home
              </span>
            </Link>
            <Link
              to="/calendar"
              type="button"
              onClick={() => setShowEventModal(false)}
              className={`inline-flex flex-col items-center justify-center px-12 rounded-full h-14 group ${
                darkMode ? 'hover:bg-black' : 'hover:bg-zinc-200'
              }`}>
              <svg
                className={`w-6 h-6 mb-1 ${
                  darkMode
                    ? 'text-zinc-200 group-hover:text-cyan-400'
                    : 'text-zinc-500 group-hover:text-cyan-600'
                }`}
                fill="currentColor"
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
                aria-hidden="true">
                <path d="M12.75 12.75a.75.75 0 11-1.5 0 .75.75 0 011.5 0zM7.5 15.75a.75.75 0 100-1.5.75.75 0 000 1.5zM8.25 17.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zM9.75 15.75a.75.75 0 100-1.5.75.75 0 000 1.5zM10.5 17.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zM12 15.75a.75.75 0 100-1.5.75.75 0 000 1.5zM12.75 17.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zM14.25 15.75a.75.75 0 100-1.5.75.75 0 000 1.5zM15 17.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zM16.5 15.75a.75.75 0 100-1.5.75.75 0 000 1.5zM15 12.75a.75.75 0 11-1.5 0 .75.75 0 011.5 0zM16.5 13.5a.75.75 0 100-1.5.75.75 0 000 1.5z" />
                <path
                  fillRule="evenodd"
                  d="M6.75 2.25A.75.75 0 017.5 3v1.5h9V3A.75.75 0 0118 3v1.5h.75a3 3 0 013 3v11.25a3 3 0 01-3 3H5.25a3 3 0 01-3-3V7.5a3 3 0 013-3H6V3a.75.75 0 01.75-.75zm13.5 9a1.5 1.5 0 00-1.5-1.5H5.25a1.5 1.5 0 00-1.5 1.5v7.5a1.5 1.5 0 001.5 1.5h13.5a1.5 1.5 0 001.5-1.5v-7.5z"
                  clipRule="evenodd"
                />
              </svg>
              <span
                className={`text-sm ${
                  darkMode
                    ? 'text-zinc-200 group-hover:text-cyan-400'
                    : 'text-zinc-500 group-hover:text-cyan-600'
                }`}>
                Calendar
              </span>
            </Link>
            <Link
              to="/events"
              type="button"
              className={`inline-flex flex-col items-center justify-center px-12 rounded-full h-14 group ${
                darkMode ? 'hover:bg-black' : 'hover:bg-zinc-200'
              }`}>
              <svg
                className={`w-6 h-6 mb-1 ${
                  darkMode
                    ? 'text-zinc-200 group-hover:text-cyan-400'
                    : 'text-zinc-500 group-hover:text-cyan-600'
                }`}
                fill="currentColor"
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
                aria-hidden="true">
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M15.362 5.214A8.252 8.252 0 0112 21 8.25 8.25 0 016.038 7.048 8.287 8.287 0 009 9.6a8.983 8.983 0 013.361-6.867 8.21 8.21 0 003 2.48z"
                />
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M12 18a3.75 3.75 0 00.495-7.467 5.99 5.99 0 00-1.925 3.546 5.974 5.974 0 01-2.133-1A3.75 3.75 0 0012 18z"
                />
              </svg>
              <span
                className={`text-sm ${
                  darkMode
                    ? 'text-zinc-200 group-hover:text-cyan-400'
                    : 'text-zinc-500 group-hover:text-cyan-600'
                }`}>
                Events
              </span>
            </Link>
            <Link
              to="/contacts"
              type="button"
              className={`inline-flex flex-col items-center justify-center px-12 rounded-full h-14 group ${
                darkMode ? 'hover:bg-black' : 'hover:bg-zinc-200'
              }`}>
              <svg
                className={`w-6 h-6 mb-1 ${
                  darkMode
                    ? 'text-zinc-200 group-hover:text-cyan-400'
                    : 'text-zinc-500 group-hover:text-cyan-600'
                }`}
                fill="currentColor"
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
                aria-hidden="true">
                <path d="M4.5 6.375a4.125 4.125 0 118.25 0 4.125 4.125 0 01-8.25 0zM14.25 8.625a3.375 3.375 0 116.75 0 3.375 3.375 0 01-6.75 0zM1.5 19.125a7.125 7.125 0 0114.25 0v.003l-.001.119a.75.75 0 01-.363.63 13.067 13.067 0 01-6.761 1.873c-2.472 0-4.786-.684-6.76-1.873a.75.75 0 01-.364-.63l-.001-.122zM17.25 19.128l-.001.144a2.25 2.25 0 01-.233.96 10.088 10.088 0 005.06-1.01.75.75 0 00.42-.643 4.875 4.875 0 00-6.957-4.611 8.586 8.586 0 011.71 5.157v.003z" />
              </svg>
              <span
                className={`text-sm ${
                  darkMode
                    ? 'text-zinc-200 group-hover:text-cyan-400'
                    : 'text-zinc-500 group-hover:text-cyan-600'
                }`}>
                Contacts
              </span>
            </Link>
          </div>
        </div>
      )}
    </>
  );
}

export default NavBar;
