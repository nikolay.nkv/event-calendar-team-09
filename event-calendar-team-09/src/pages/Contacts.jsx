import { useState, useEffect } from 'react';
import {
  getUserByHandle,
  getUserData,
  createCategory,
  addContactToCategory,
  getCategoryData,
  updateCategoryData
} from '../services/userService';
import { useAuth } from '../services/firebase';
import { getDownloadURL, ref } from 'firebase/storage';
import { storage } from '../services/firebase';
import { Link } from 'react-router-dom';
import { toggleContact } from '../services/userService';

function ContactsList() {
  const currentUser = useAuth();
  const [userProfile, setUserProfile] = useState(null);
  const [profilePhotoUrls, setProfilePhotoUrls] = useState({});
  const [contactDataArray, setContactDataArray] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const defaultPlaceholderUrl =
    'https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png';
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);
  const [contactToRemove, setContactToRemove] = useState(null);
  const [isCreatingCategory, setIsCreatingCategory] = useState(false);
  const [newCategoryName, setNewCategoryName] = useState('');
  const [selectedContacts, setSelectedContacts] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState('All');
  const [filteredContactsByCategoryAndSearch, setFilteredContactsByCategoryAndSearch] = useState(
    []
  );
  const [showSuccessAlert, setShowSuccessAlert] = useState(false);

  const openDeleteModal = (contact) => {
    setContactToRemove(contact);
    setIsDeleteModalOpen(true);
  };

  const closeDeleteModal = () => {
    setContactToRemove(null);
    setIsDeleteModalOpen(false);
  };

  const displaySuccessAlert = () => {
    setShowSuccessAlert(true);

    setTimeout(() => {
      setShowSuccessAlert(false);
    }, 3000);
  };

  async function fetchUserProfile(handle) {
    try {
      const userProfileSnapshot = await getUserByHandle(handle);
      const userProfileData = userProfileSnapshot.val();
      return userProfileData;
    } catch (error) {
      console.error('Error fetching user profile:', error);
      return null;
    }
  }

  useEffect(() => {
    async function fetchUserProfileData() {
      if (currentUser) {
        const fetchedUserProfile = await fetchUserProfile(currentUser.handle);
        setUserProfile(fetchedUserProfile);
      }
    }

    fetchUserProfileData();
  }, [currentUser]);

  useEffect(() => {
    if (currentUser) {
      getUserData(currentUser.uid)
        .then((snapshot) => {
          if (snapshot.exists()) {
            const userData = snapshot.val();
            const firstUser = Object.values(userData)[0];
            setUserProfile(firstUser);
          }
        })
        .catch((error) => {
          console.error('Error fetching user data:', error);
        });
    }
  }, [currentUser]);

  useEffect(() => {
    if (userProfile) {
      const userContacts = userProfile.contacts || {};
      const contactHandles = Object.keys(userContacts);

      const fetchContactData = async () => {
        const contactDataArray = await Promise.all(
          contactHandles.map(async (contactHandle) => {
            const contactDataSnapshot = await getUserByHandle(contactHandle);
            if (contactDataSnapshot.exists()) {
              const contactData = contactDataSnapshot.val();
              const profilePhotoUrl = contactData.profilePhotoUrl || defaultPlaceholderUrl;

              if (contactData.uid) {
                try {
                  const userPhotoRef = ref(storage, `${contactData.uid}.png`);
                  const url = await getDownloadURL(userPhotoRef);

                  return {
                    ...contactData,
                    profilePhotoUrl: url
                  };
                } catch (error) {
                  console.error(`Error fetching profile photo for ${contactData.uid}:`, error);
                }
              }

              return {
                ...contactData,
                profilePhotoUrl
              };
            }
            return null;
          })
        );

        const filteredContactDataArray = contactDataArray.filter((data) => data !== null);
        setContactDataArray(filteredContactDataArray);

        const profilePhotoUrlMap = filteredContactDataArray.reduce((acc, contactData) => {
          if (contactData.uid && contactData.profilePhotoUrl) {
            acc[contactData.uid] = contactData.profilePhotoUrl;
          }
          return acc;
        }, {});
        setProfilePhotoUrls(profilePhotoUrlMap);
      };

      fetchContactData();
    }
  }, [userProfile]);

  const filteredContacts = contactDataArray.filter((contact) => {
    const searchRegex = new RegExp(searchTerm, 'i');
    return (
      searchRegex.test(contact.handle) ||
      searchRegex.test(contact.firstName) ||
      searchRegex.test(contact.lastName)
    );
  });

  const removeContactFromCategory = async (userHandle, categoryId, contactHandle) => {
    try {
      const categorySnapshot = await getCategoryData(userHandle, categoryId);
      const categoryData = categorySnapshot.val();

      if (categoryData && categoryData.contacts && categoryData.contacts[contactHandle]) {
        delete categoryData.contacts[contactHandle];

        await updateCategoryData(userHandle, categoryId, categoryData);
      }
    } catch (error) {
      console.error('Error removing contact from category:', error);
      throw error;
    }
  };

  const removeContact = async () => {
    if (contactToRemove) {
      try {
        const removedContactHandle = contactToRemove.handle;

        await toggleContact(userProfile.handle, removedContactHandle, 'remove');

        if (userProfile && userProfile.categories) {
          for (const categoryId in userProfile.categories) {
            const category = userProfile.categories[categoryId];
            if (category && category.contacts && category.contacts[removedContactHandle]) {
              await removeContactFromCategory(userProfile.handle, categoryId, removedContactHandle);
            }
          }
        }

        setContactDataArray((prevContacts) =>
          prevContacts.filter((contact) => contact.handle !== removedContactHandle)
        );

        setFilteredContactsByCategoryAndSearch((prevContacts) =>
          prevContacts.filter((contact) => contact.handle !== removedContactHandle)
        );

        closeDeleteModal();
      } catch (error) {
        console.error('Error removing contact:', error);
      }
    }
  };

  const startCategoryCreation = () => {
    setIsCreatingCategory(true);
  };

  const cancelCategoryCreation = () => {
    setIsCreatingCategory(false);
    setNewCategoryName('');
    setSelectedContacts([]);
  };

  const handleContactSelection = (contactHandle) => {
    if (selectedContacts.includes(contactHandle)) {
      setSelectedContacts(selectedContacts.filter((handle) => handle !== contactHandle));
    } else {
      setSelectedContacts([...selectedContacts, contactHandle]);
    }
  };

  const handleCreateCategory = async () => {
    try {
      const newCategoryId = await createCategory(userProfile.handle, newCategoryName);
      console.log('New category ID:', newCategoryId);

      await Promise.all(
        selectedContacts.map(async (contactHandle) => {
          await addContactToCategory(userProfile.handle, newCategoryId, contactHandle);
        })
      );

      setIsCreatingCategory(false);
      setNewCategoryName('');
      setSelectedContacts([]);

      displaySuccessAlert();
    } catch (error) {
      console.error('Error creating category:', error);
    }
  };

  const handleCategoryChange = (e) => {
    const selectedCategory = e.target.value;
    setSelectedCategory(selectedCategory);
  };

  useEffect(() => {
    const updatedFilteredContacts = async () => {
      let contactHandles = [];

      if (selectedCategory !== 'All' && userProfile && userProfile.categories) {
        const category = userProfile.categories[selectedCategory];
        if (category && category.contacts) {
          contactHandles.push(...Object.keys(category.contacts));
        }
      } else {
        contactHandles = Object.keys(userProfile.contacts || {});
      }

      const contactDataArray = await Promise.all(
        contactHandles.map(async (contactHandle) => {
          const contactDataSnapshot = await getUserByHandle(contactHandle);
          if (contactDataSnapshot.exists()) {
            const contactData = contactDataSnapshot.val();
            return {
              ...contactData,
              profilePhotoUrl: contactData.profilePhotoUrl || defaultPlaceholderUrl
            };
          }
          return null;
        })
      );

      const filteredByCategory = contactDataArray.filter((data) => data !== null);

      const searchRegex = new RegExp(searchTerm, 'i');
      const filteredBySearch = filteredByCategory.filter((contact) => {
        return (
          searchRegex.test(contact.handle) ||
          searchRegex.test(contact.firstName) ||
          searchRegex.test(contact.lastName)
        );
      });

      setFilteredContactsByCategoryAndSearch(filteredBySearch);
    };

    updatedFilteredContacts();
  }, [selectedCategory, userProfile, searchTerm]);

  return (
    <>
      <div className="min-h-screen bg-white dark:bg-zinc-900 p-8 w-full">
        {showSuccessAlert && (
          <div
            className="flex items-center p-4 mb-4 text-sm text-green-800 border border-green-300 rounded-lg bg-green-50 dark:bg-gray-800 dark:text-green-400 dark:border-green-800"
            role="alert">
            <svg
              className="flex-shrink-0 inline w-4 h-4 mr-3"
              aria-hidden="true"
              xmlns="http://www.w3.org/2000/svg"
              fill="currentColor"
              viewBox="0 0 20 20">
              <path d="M10 .5a9.5 9.5 0 1 0 9.5 9.5A9.51 9.51 0 0 0 10 .5ZM9.5 4a1.5 1.5 0 1 1 0 3 1.5 1.5 0 0 1 0-3ZM12 15H8a1 1 0 0 1 0-2h1v-3H8a1 1 0 0 1 0-2h2a1 1 0 0 1 1 1v4h1a1 1 0 0 1 0 2Z" />
            </svg>
            <span className="sr-only">Info</span>
            <div>
              <span className="font-medium">Success alert!</span> New contacts category was created.
            </div>
          </div>
        )}
        <div className="flex items-center justify-between pb-6">
          <div>
            <h2 className="text-zinc-700 dark:text-zinc-200 font-semibold text-2xl">Contacts</h2>
            <span className="text-base text-zinc-500 dark:text-zinc-400">
              {selectedCategory === 'All'
                ? 'All contacts'
                : (userProfile.categories[selectedCategory]?.name || 'Unknown Category') +
                  ' contacts'}
            </span>
          </div>
          <div className="flex items-center w-1/3 p-2.5 rounded-full shadow shadow-zinc-300 dark:shadow-black focus-within:shadow-teal-600 dark:focus-within:shadow-teal-600 bg-white dark:bg-zinc-900 overflow-hidden">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-5 w-5 text-zinc-500 dark:text-zinc-200"
              viewBox="0 0 20 20"
              fill="currentColor">
              <path
                fillRule="evenodd"
                d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                clipRule="evenodd"
              />
            </svg>
            <input
              className="outline-none ml-1 block bg-white dark:bg-zinc-900 text-zinc-700 dark:text-zinc-300 dark:placeholder-zinc-500 placeholder-zinc-500"
              type="text"
              placeholder="Search contact..."
              value={searchTerm}
              onChange={(e) => setSearchTerm(e.target.value)}
            />
          </div>
          <div className="flex items-center">
            <div className="relative inline-flex">
              <svg
                className="w-2 h-2 absolute top-0 right-0 m-4 pointer-events-none"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 412 232">
                <path
                  d="M206 171.144L42.678 7.822c-9.763-9.763-25.592-9.763-35.355 0-9.763 9.764-9.763 25.592 0 35.355l181 181c4.88 4.882 11.279 7.323 17.677 7.323s12.796-2.441 17.678-7.322l181-181c9.763-9.764 9.763-25.592 0-35.355-9.763-9.763-25.592-9.763-35.355 0L206 171.144z"
                  fill="#648299"
                  fillRule="nonzero"
                />
              </svg>
              <select
                className=" text-zinc-600 dark:text-zinc-400 h-10 pl-5 pr-10 bg-white dark:bg-zinc-900 focus:outline-none appearance-none text-base"
                value={selectedCategory}
                onChange={handleCategoryChange}>
                <option value="All">All Categories</option>
                {userProfile &&
                  userProfile.categories &&
                  Object.keys(userProfile.categories).map((categoryId) => (
                    <option key={categoryId} value={categoryId}>
                      {userProfile.categories[categoryId].name}
                    </option>
                  ))}
              </select>
            </div>
            <div className="ml-4 space-x-4">
              <Link
                to="/search"
                className="bg-teal-500 dark:bg-teal-400 hover:bg-gray-100 dark:hover:bg-zinc-900 text-white dark:text-zinc-900 hover:text-teal-500 dark:hover:text-teal-400 border border-transparent hover:border-teal-500 dark:hover:border-teal-400  rounded-full px-4 py-3 text-base font-bold">
                + Contact
              </Link>
              <button
                className="bg-cyan-500  dark:bg-cyan-400 hover:bg-gray-100 dark:hover:bg-zinc-900 text-white dark:text-zinc-900 hover:text-cyan-500 dark:hover:text-cyan-400 border border-transparent hover:border-cyan-500 dark:hover:border-cyan-400 rounded-full px-4 py-2.5 text-base font-bold"
                onClick={startCategoryCreation}>
                + Create category
              </button>
            </div>
          </div>
        </div>
        <div className="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
          <div className="inline-block min-w-full shadow-md shadow-zinc-400 dark:shadow-black rounded-lg overflow-hidden">
            <table className="min-w-full leading-normal">
              <thead>
                <tr>
                  <th className="px-5 py-5 border-l border-b border-zinc-200 bg-zinc-100 dark:border-zinc-950 dark:bg-zinc-950 text-left text-sm font-semibold text-zinc-600 dark:text-zinc-300 uppercase tracking-wider">
                    Name
                  </th>
                  <th className="px-5 py-5 border-b border-zinc-200 bg-zinc-100 dark:border-zinc-950 dark:bg-zinc-950 text-left text-sm font-semibold text-zinc-600 dark:text-zinc-300 uppercase tracking-wider">
                    Role
                  </th>
                  <th className="px-5 py-5 border-b border-zinc-200 bg-zinc-100 dark:border-zinc-950 dark:bg-zinc-950 text-left text-sm font-semibold text-zinc-600 dark:text-zinc-300 uppercase tracking-wider">
                    Email
                  </th>
                  <th className="px-5 py-5 border-b border-zinc-200 bg-zinc-100 dark:border-zinc-950 dark:bg-zinc-950 text-left text-sm font-semibold text-zinc-600 dark:text-zinc-300 uppercase tracking-wider">
                    Phone
                  </th>
                  <th className="px-5 py-5 border-r border-b border-zinc-200 bg-zinc-100 dark:border-zinc-950 dark:bg-zinc-950 text-left text-sm font-semibold text-zinc-600 dark:text-zinc-300 uppercase tracking-wider">
                    Remove
                  </th>
                </tr>
              </thead>
              <tbody>
                {filteredContactsByCategoryAndSearch.map((contact) => (
                  <tr key={contact.handle}>
                    <td className="px-5 py-5 border-b border-l border-gray-200 bg-white text-base dark:border-zinc-950 dark:bg-zinc-900">
                      <div className="flex items-center">
                        <div className="flex-shrink-0 w-10 h-10">
                          <img
                            className="w-full h-full rounded-full"
                            src={profilePhotoUrls[contact.uid]}
                            alt={`Profile of ${contact.firstName} ${contact.lastName}`}
                          />
                        </div>
                        <div className="ml-3">
                          <p className="text-zinc-900 dark:text-zinc-300 whitespace-no-wrap">
                            {contact.firstName} {contact.lastName}
                          </p>
                        </div>
                      </div>
                    </td>
                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-base dark:border-zinc-950 dark:bg-zinc-900">
                      <p className="text-zinc-900 dark:text-zinc-300 whitespace-no-wrap">
                        {contact.role}
                      </p>
                    </td>
                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-base dark:border-zinc-950 dark:bg-zinc-900">
                      <p className="text-zinc-900 dark:text-zinc-300 whitespace-no-wrap">
                        {contact.email}
                      </p>
                    </td>
                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-base dark:border-zinc-950 dark:bg-zinc-900">
                      <p className="text-zinc-900 dark:text-zinc-300 whitespace-no-wrap">
                        {contact.phoneNumber}
                      </p>
                    </td>
                    <td className="px-5 py-5 border-b border-r border-gray-200 bg-white text-base dark:border-zinc-950 dark:bg-zinc-900">
                      <button
                        className="relative rounded-lg px-3 py-3 overflow-hidden group
              bg-rose-500 hover:bg-gradient-to-r hover:from-rose-500 hover:to-rose-400 text-white hover:ring-2 hover:ring-offset-2 hover:ring-offset-white dark:hover:ring-offset-zinc-950 hover:ring-rose-400 ml-3"
                        onClick={() => openDeleteModal(contact)}>
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                          strokeWidth={1.5}
                          stroke="currentColor"
                          className="w-4 h-4">
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"
                          />
                        </svg>
                      </button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
            <div className="px-5 py-4 bg-zinc-100 border-r border-l dark:bg-zinc-950 border-zinc-200 dark:border-zinc-950 flex flex-col xs:flex-row items-center xs:justify-between">
              <span className="text-xs xs:text-sm text-zinc-700 dark:text-zinc-400">
                Showing 1 to {filteredContactsByCategoryAndSearch.length} of{' '}
                {filteredContactsByCategoryAndSearch.length} Entries
              </span>
            </div>
          </div>
        </div>
      </div>
      <div
        id="deleteModal"
        tabIndex="-1"
        aria-hidden={!isDeleteModalOpen}
        className={`${
          isDeleteModalOpen ? 'fixed' : 'hidden'
        } inset-0 overflow-y-auto flex items-center justify-center z-50`}>
        <div className="fixed inset-0 bg-black bg-opacity-60"></div>
        <div className="relative p-4 w-full max-w-md h-full md:h-auto">
          <div className="relative p-4 text-center bg-white rounded-lg shadow dark:bg-zinc-900 sm:p-5">
            <button
              type="button"
              className="text-zinc-400 dark:text-zinc-200 absolute top-2.5 right-2.5 bg-transparent hover:bg-zinc-200 hover:text-zinc-700 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-zinc-600 dark:hover:text-white"
              onClick={() => closeDeleteModal()}
              data-modal-toggle="deleteModal">
              <svg
                aria-hidden="true"
                className="w-5 h-5"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg">
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"></path>
              </svg>
              <span className="sr-only">Close modal</span>
            </button>
            <svg
              className="text-zinc-400 dark:text-zinc-300 w-11 h-11 mb-3.5 mx-auto"
              aria-hidden="true"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg">
              <path
                fillRule="evenodd"
                d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z"
                clipRule="evenodd"></path>
            </svg>
            <p className="mb-8 text-zinc-500 dark:text-zinc-300">
              Are you sure you want to remove {contactToRemove && contactToRemove.handle} from
              contacts?
            </p>
            <div className="flex justify-center items-center space-x-6">
              <button
                data-modal-toggle="deleteModal"
                type="button"
                className="py-2 px-3 text-sm font-medium text-zinc-500 bg-white rounded-lg border border-zinc-500 hover:bg-zinc-600 hover:text-zinc-50 dark:bg-zinc-800 dark:text-zinc-200 dark:border-zinc-400 dark:hover:text-zinc-950 dark:hover:bg-zinc-100"
                onClick={() => closeDeleteModal()}>
                No, cancel
              </button>
              <button
                type="submit"
                className="py-2 px-3 text-sm font-medium text-center text-white bg-rose-500 rounded-lg border border-rose-500 hover:bg-rose-600 dark:border-rose-500 dark:hover:border-rose-700 dark:bg-red-500 dark:hover:bg-red-700"
                onClick={() => removeContact()}>
                Yes, I&#39;m sure
              </button>
            </div>
          </div>
        </div>
      </div>
      {isCreatingCategory && (
        <div className="fixed inset-0 bg-black bg-opacity-60">
          <div className="absolute inset-0 flex items-center justify-center">
            <div className="bg-white dark:bg-zinc-900 p-12 rounded-lg">
              <h2 className="text-xl font-semibold mb-4 text-zinc-700 dark:text-zinc-200">
                Create a Category
              </h2>
              <input
                type="text"
                className="w-full border-2 text-zinc-900 dark:text-zinc-200 border-white dark:border-zinc-900 bg-zinc-100 dark:bg-zinc-800 p-2 mb-8 rounded-lg hover:border-teal-500 active:border-teal-500 dark:hover:border-teal-500 dark:active:border-teal-500 placeholder:text-zinc-500"
                placeholder="Category Name"
                value={newCategoryName}
                onChange={(e) => setNewCategoryName(e.target.value)}
              />
              <div className="mb-8">
                <p className="text-lg font-semibold mb-2 text-zinc-700 dark:text-zinc-300">
                  Select Contacts:
                </p>
                {filteredContacts.map((contact) => (
                  <label
                    key={contact.handle}
                    className="flex items-center mt-4 ml-2 mb-4 text-md cursor-pointer relative">
                    <img
                      src={contact.profilePhotoUrl}
                      alt={`Profile of ${contact.firstName} ${contact.lastName}`}
                      className="w-8 h-8 rounded-full mr-2"
                    />
                    <span className="text-zinc-700 dark:text-zinc-300">
                      {contact.firstName} {contact.lastName}
                    </span>
                    <input
                      type="checkbox"
                      className="checkbox cursor-pointer h-0 w-0 appearance-none"
                      style={{ display: 'none' }}
                      value={contact.handle}
                      checked={selectedContacts.includes(contact.handle)}
                      onChange={() => handleContactSelection(contact.handle)}
                    />
                    <span
                      className={`ml-3 top-2 text-2xl font-bold ${
                        selectedContacts.includes(contact.handle)
                          ? 'text-rose-500 dark:text-rose-400'
                          : 'text-teal-700 dark:text-teal-400'
                      }`}>
                      {selectedContacts.includes(contact.handle) ? (
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                          strokeWidth={1.5}
                          stroke="currentColor"
                          className="w-6 h-6">
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M15 12H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"
                          />
                        </svg>
                      ) : (
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                          strokeWidth={1.5}
                          stroke="currentColor"
                          className="w-6 h-6">
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"
                          />
                        </svg>
                      )}
                    </span>
                  </label>
                ))}
              </div>

              <div className="flex justify-end">
                <button
                  className="flex items-center justify-center mr-2 px-4 py-2 text-base font-medium leading-6 text-white whitespace-no-wrap bg-rose-400 border-2 border-transparent rounded-full shadow-sm hover:bg-transparent hover:text-rose-400 hover:border-rose-400 focus:outline-none"
                  onClick={() => cancelCategoryCreation()}>
                  Cancel
                </button>
                <button
                  className="flex items-center justify-center px-4 py-2 text-base font-medium leading-6 text-white whitespace-no-wrap bg-teal-500 border-2 border-transparent rounded-full shadow-sm hover:bg-transparent hover:text-teal-500 hover:border-teal-500 focus:outline-none"
                  onClick={handleCreateCategory}>
                  Create Category
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
}

export default ContactsList;
