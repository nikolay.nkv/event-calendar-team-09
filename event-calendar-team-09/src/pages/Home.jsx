import Weather from '../components/weather/Weather';
import Calendar from '../components/calendar/MonthCalendar/Calendar';

export const Home = () => {
  return (
    <>
      <div className="bg-zinc-200 dark:bg-zinc-900 pt-4">
        <div>
          <Weather />
        </div>
        <div>
          <Calendar />
        </div>
      </div>
    </>
  );
};
