import { ref, push, get, query, equalTo, orderByChild, update, remove } from 'firebase/database';
import { db } from '../services/firebase.js';
import { addEventToUser, getUserByHandle } from './userService.js';

export const fromEventsDocument = (snapshot) => {
  const eventsDocument = snapshot.val();

  return Object.keys(eventsDocument).map((key) => {
    const event = eventsDocument[key];

    return {
      ...event,
      id: key,
      createdOn: new Date(event.createdOn)
    };
  });
};

export const addEvent = (
  title,
  description,
  startDate,
  endDate,
  address,
  isPublic,
  author,
  participants,
  category,
  selectedWeekdays,
  selectedMonthDays,
  recurrencePattern
) => {
  return push(ref(db, 'events'), {
    title,
    description,
    startDate,
    endDate,
    address,
    isPublic,
    author,
    participants,
    category,
    selectedWeekdays,
    selectedMonthDays,
    recurrencePattern
  }).then((result) => {
    return getEventById(result.key);
  });
};

export const getEventById = (eventId) => {
  return get(ref(db, `events/${eventId}`)).then((snapshot) => {
    if (!snapshot.exists()) {
      throw new Error(`Event with id ${eventId} does not exist!`);
    }
    const event = snapshot.val();
    event.id = eventId;
    event.title = '';
    event.startDate = new Date(event.startDate);
    event.endDate = new Date(event.endDate);
    if (!event.participants) {
      event.participants = [];
    }
    return event;
  });
};

export const getAllEvents = () => {
  return get(ref(db, 'events')).then((snapshot) => {
    if (!snapshot.exists()) {
      return [];
    }

    return fromEventsDocument(snapshot);
  });
};

export const getEventsByAuthor = (uid) => {
  return get(query(ref(db, 'events'), orderByChild('author'), equalTo(uid))).then((snapshot) => {
    if (!snapshot.exists()) return [];

    return fromEventsDocument(snapshot);
  });
};

export const getEventsByCategory = (category) => {
  return get(query(ref(db, 'events'), orderByChild('category'), equalTo(category))).then(
    (snapshot) => {
      if (!snapshot.exists()) return [];

      return fromEventsDocument(snapshot);
    }
  );
};

export const updateEvent = (eventId, newTitle, newDescription, newDate, newTheme, newCategory) => {
  const eventRef = ref(db, `events/${eventId}`);
  return update(eventRef, {
    title: newTitle,
    description: newDescription,
    date: newDate,
    theme: newTheme,
    category: newCategory
  });
};

export const addParticipants = (handle, eventId) => {
  const updateParticipants = {};
  updateParticipants[`/events/${eventId}/participants/${handle}`] = true;
  updateParticipants[`/users/${handle}/myEvents/${eventId}`] = true;

  return update(ref(db), updateParticipants);
};

export const changeTitle = (eventId, newTitle) => {
  const eventRef = ref(db, `events/${eventId}`);
  return update(eventRef, {
    title: newTitle
  });
};

export const changeDescription = (eventId, newDescription) => {
  const eventRef = ref(db, `events/${eventId}`);
  return update(eventRef, {
    description: newDescription
  });
};

export const changeStartDate = (eventId, newStartdate) => {
  const eventRef = ref(db, `events/${eventId}`);
  return update(eventRef, {
    startDate: newStartdate
  });
};

export const changeEndDate = (eventId, newEndDate) => {
  const eventRef = ref(db, `events/${eventId}`);
  return update(eventRef, {
    newEndDate: newEndDate
  });
};

export const changeCategory = (eventId, newCategory) => {
  const eventRef = ref(db, `events/${eventId}`);
  return update(eventRef, {
    category: newCategory
  });
};

export const changeAddress = (eventId, newAddress) => {
  const eventRef = ref(db, `events/${eventId}`);
  return update(eventRef, {
    address: newAddress
  });
};

export const changeRecurrencePattern = (eventId, newReccurrencePattern) => {
  const eventRef = ref(db, `events/${eventId}`);
  return update(eventRef, {
    recurrencePattern: newReccurrencePattern
  });
};

export const changeSelectedMounthDays = (eventId, newSelectedMounthDays) => {
  const eventRef = ref(db, `events/${eventId}`);
  return update(eventRef, {
    selectedMonthDays: newSelectedMounthDays
  });
};

export const changeSelectedWeekDays = (eventId, newSelectedWeekDays) => {
  const eventRef = ref(db, `events/${eventId}`);
  return update(eventRef, {
    selectedWeekdays: newSelectedWeekDays
  });
};

export const toggleEventParticipant = async (event, participantHandle, action) => {
  try {
    const participantSnapshot = await getUserByHandle(participantHandle);

    const eventParticipants = event.participants || {};
    if (action === 'add') {
      eventParticipants[participantHandle] = true;
    } else if (action === 'remove') {
      delete eventParticipants[participantHandle];
    }
    await addParticipants(event, eventParticipants);

    const participantEvents = participantSnapshot.val().myEvents || {};
    if (action === 'add') {
      participantEvents[event] = true;
    } else if (action === 'remove') {
      delete participantEvents[event];
    }
    await addEventToUser(participantHandle, participantEvents);
  } catch (error) {
    console.error('Error updating participants:', error);
  }
};

export const deleteEvent = (event) => {
  remove(ref(db, `events/${event}`))
    .then(() => {
      console.log('Event deleted successfully.');
    })
    .catch((error) => {
      console.error('Error deleting event:', error);
    });
};
