import { get, set, ref, query, orderByChild, equalTo, update, push } from 'firebase/database';
import { db } from '../services/firebase';

export const getUserByHandle = (handle) => {
  return get(ref(db, `users/${handle}`));
};

export const createUserHandle = (
  handle,
  uid,
  email,
  firstName,
  lastName,
  phoneNumber,
  myEvents = {},
  role = 'regular',
  blocked = false
) => {
  return set(ref(db, `users/${handle}`), {
    uid,
    email,
    firstName,
    handle,
    lastName,
    phoneNumber,
    myEvents,
    role,
    blocked,
    createdOn: Date.now()
  });
};

export const getUserData = (uid) => {
  return get(query(ref(db, 'users'), orderByChild('uid'), equalTo(uid)));
};

export const changeFirstName = (handle, newFirstName) => {
  const userRef = ref(db, `users/${handle}`);
  return update(userRef, {
    firstName: newFirstName
  });
};

export const changeLastName = (handle, newLastName) => {
  const userRef = ref(db, `users/${handle}`);
  return update(userRef, {
    lastName: newLastName
  });
};

export const changePhoneNumber = (handle, newPhoneNumber) => {
  const userRef = ref(db, `users/${handle}`);
  return update(userRef, {
    phoneNumber: newPhoneNumber
  });
};

export const changeAddress = (handle, newAddress) => {
  const userRef = ref(db, `users/${handle}`);
  return update(userRef, {
    address: newAddress
  });
};

export const getAllUsers = async () => {
  const usersRef = ref(db, 'users');
  const usersSnapshot = await get(usersRef);

  if (usersSnapshot.exists()) {
    const userData = usersSnapshot.val();
    const userList = Object.values(userData);
    return userList;
  } else {
    return [];
  }
};

export const updateUserRoleInFirebase = (handle, newRole) => {
  const userRef = ref(db, `users/${handle}`);
  return update(userRef, {
    role: newRole
  });
};

export const blockUser = (handle) => {
  const userRef = ref(db, `users/${handle}`);
  return update(userRef, {
    blocked: true
  });
};

export const unblockUser = (handle) => {
  const userRef = ref(db, `users/${handle}`);
  return update(userRef, {
    blocked: false
  });
};

export const toggleContact = async (currentUserHandle, contactHandle, action) => {
  try {
    const currentUserSnapshot = await getUserByHandle(currentUserHandle);
    const contactUserSnapshot = await getUserByHandle(contactHandle);

    if (!currentUserSnapshot.exists() || !contactUserSnapshot.exists()) {
      console.log('User not found');
      return;
    }

    const currentUserContacts = currentUserSnapshot.val().contacts || {};
    if (action === 'add') {
      currentUserContacts[contactHandle] = true;
    } else if (action === 'remove') {
      delete currentUserContacts[contactHandle];
    }
    await updateContact(currentUserHandle, currentUserContacts);

    const contactUserContacts = contactUserSnapshot.val().contacts || {};
    if (action === 'add') {
      contactUserContacts[currentUserHandle] = true;
    } else if (action === 'remove') {
      delete contactUserContacts[currentUserHandle];
    }
    await updateContact(contactHandle, contactUserContacts);

    console.log('Contacts updated successfully');
  } catch (error) {
    console.error('Error updating contacts:', error);
  }
};

export const updateContact = (userHandle, contacts) => {
  const userRef = ref(db, `users/${userHandle}`);
  return update(userRef, { contacts });
};

export const createCategory = async (userHandle, categoryName) => {
  try {
    const userCategoriesRef = ref(db, `users/${userHandle}/categories`);
    const newCategoryId = push(userCategoriesRef).key;

    const newCategory = {
      createdOn: Date.now(),
      name: categoryName,
      contacts: {}
    };

    await update(userCategoriesRef, { [newCategoryId]: newCategory });

    console.log('Category created successfully');
    return newCategoryId;
  } catch (error) {
    console.error('Error creating category:', error);
    throw error;
  }
};

export const updateUserCategory = async (userHandle, newCategory) => {
  try {
    const userRef = ref(db, `users/${userHandle}`);
    await userRef.child('categories').push(newCategory);
  } catch (error) {
    console.error('Error updating user category:', error);
  }
};

export const addContactToCategory = async (userHandle, categoryId, contactHandle) => {
  try {
    const categoryRef = ref(
      db,
      `users/${userHandle}/categories/${categoryId}/contacts/${contactHandle}`
    );

    await set(categoryRef, true);

    console.log('Contact added to category successfully');
  } catch (error) {
    console.error('Error adding contact to category:', error);
    throw error;
  }
};

export const getCategoryData = async (userHandle, categoryId) => {
  try {
    const categoryRef = ref(db, `users/${userHandle}/categories/${categoryId}`);
    const categorySnapshot = await get(categoryRef);

    if (categorySnapshot.exists()) {
      return categorySnapshot;
    }

    return null;
  } catch (error) {
    console.error('Error fetching category data:', error);
    throw error;
  }
};

export const updateCategoryData = async (userHandle, categoryId, categoryData) => {
  try {
    const categoryRef = ref(db, `users/${userHandle}/categories/${categoryId}`);
    await set(categoryRef, categoryData);
  } catch (error) {
    console.error('Error updating category data:', error);
    throw error;
  }
};

export const addEventToMyEvents = async (userHandle, eventId) => {
  try {
    const myEventsRef = ref(db, `users/${userHandle}/myEvents/${eventId}}`);

    await set(myEventsRef, true);

    console.log('Event added successfully');
  } catch (error) {
    console.error('Error adding event to user:', error);
    throw error;
  }
};

export const addEventToUser = async (userHandle, eventId) => {
  try {
    const userMyEventsRef = ref(db, `users/${userHandle}/myEvents`);
    const newEventId = push(userMyEventsRef).key;

    const newMyEvent = {
      createdOn: Date.now(),
      id: eventId
    };

    await update(userMyEventsRef, { [newEventId]: newMyEvent });

    console.log('My event id created successfully');
    return newEventId;
  } catch (error) {
    console.error('Error creating category:', error);
    throw error;
  }
};
