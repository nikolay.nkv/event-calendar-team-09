export const MIN_NAME_LENGTH = 1;
export const MAX_NAME_LENGTH = 30;

export const MIN_PASSWORD_LENGTH = 8;
export const MAX_PASSWORD_LENGTH = 30;
